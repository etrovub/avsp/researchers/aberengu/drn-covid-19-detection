##########################################################################################
# (c) Copyright 2022
# The author(s):    Abel Díaz Berenguer
#                   Tanmoy Mukherjee
# Vrije Universiteit Brussel,
# Department of Electronics and Informatics (ETRO),
# Faculty of Engineering Sciences,
# VUB-ETRO,
# 2 Pleinlaan, 1050 Belgium.
# (VUB - ETRO)
#
# All right reserved
# Permission to use, copy, modify, and distribute within VUB-ETRO
# this routine/program/software and/or its documentation, or part of any one of them
# (the program) for research purpose within VUB-ETRO is hereby granted, provided:
# (1) that the copyright notice appears in all copies,
# (2) that both the copyright notice and this permission notice appear in the supporting documentation, and
# (3) that all publications based partially or completely on this program will have the main publications of "the authors" related to this
# work cited as references.
# Any other type of handling or use of the program, CAN NOT BE DONE
# WITHOUT PRIOR WRITTEN APPROVAL from the VUB-ETRO,
# including but not restricted to the following:
# (A) any re-distribution of the program to any person outside of the VUB-ETRO
# (B) any use of the program which is not academic research;
# (C) any commercial activity involving the use of the program is strictly prohibited.
# Modifications: 2018
# Disclaimer: VUB ETRO make no claim about the
# suitability of the program for any particular purpose.
# It is provided "as is" without any form of warranty or support.
##########################################################################################
import argparse
import numpy as np
import torch.nn.functional as F
import torch
import sys
import csv
import os
from timm.models import create_model
from timm.loss import *
import utils
import torch.backends.cudnn as cudnn



from einops import rearrange, repeat

def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

##########################
### SETTINGS
##########################
parser = argparse.ArgumentParser(description='Evaluation')
##########################
### Data
##########################
parser.add_argument('--input_data', metavar='input_data', type=str,
                    default='/home/abel/Project/Data/ECCV2022_CHALLENGE_MIA-19-DB/',
                    help='Path to the root folder with the training data')
parser.add_argument('--is_training', metavar='is_training', type=str2bool,
                    default=False,
                    help='Define if the model is in training or evaluations mode')
##########################
### Data preprocessing
##########################
parser.add_argument('--do_crop', metavar='is_training', type=str2bool,
                    default=False,
                    help='Define if crop or mask')

parser.add_argument('--lower_HU', metavar='is_training', type=int,
                    default=-1150,
                    help='Define if crop or mask')

parser.add_argument('--higher_HU', metavar='is_training', type=int,
                    default=350,
                    help='Define if crop or mask')

parser.add_argument('--do_data_augmentation', metavar='do_data_augmentation', type=str2bool,
                    default=False, help='To do data augmentation or not')
##########################
### Model Optimization
##########################
# # Model Exponential Moving Average
parser.add_argument('--model-ema', type=str2bool, default=False,
                    help='Test the moving average of model weights')

parser.add_argument('--checkpoint_output', metavar='checkpoint_output', type=str,
                    default='/home/abel/Project/EXP_COVID-19_V5_3/ECCV_Challenge/June2022/Logs_and_models/convnext_base/covid_severity_cls/model_pretrained_ImageNet_do_Augment_True_exp_date_06_29_2022_time_13_59_34/',
                    help='Path to the root folder to save the checkpoint')

parser.add_argument('--epoch', metavar='epoch', type=int,
                    default=143,
                    help='Best checkpoint at validation')

parser.add_argument('--label_smoothing', metavar='label_smoothing', type=float,
                    default=0.4,
                    help='Label_smoothing fro CE loss')

parser.add_argument('--eval_batchsize', type=int, default=1, metavar='batchsize',
                    help="Number of volumes to process simultaneously. Lower number requires less memory but may be slower")

parser.add_argument('--num_epochs', metavar='epochs', type=int,
                    default=143,
                    help='Define if the number of epochs to train')

##########################
### Model Parameters
##########################
parser.add_argument('--model', type=str, choices=['convnext_xlarge', 'convnext_large', 'convnext_base',  'convnext_small','convnext_tiny'],
                    help='the model to use',
                    default='convnext_base')#So far 3 models

parser.add_argument('--layer_scale_init_value', default=1e-6, type=float,
                    help="Layer scale initial values")

parser.add_argument('--head_init_scale', default=1.0, type=float,
                    help='classifier head initial scale, typically adjusted in fine-tuning')

parser.add_argument('--num_classes', type=int,
                    help="Number of classes",
                    default=4)

parser.add_argument('--in_plane_x', metavar='in_plane_size', type=int,
                    default=224, help='in_plane_size_x')

parser.add_argument('--in_plane_y', metavar='in_plane_size', type=int,
                    default=224, help='in_plane_size_y')

parser.add_argument('--number_slices', metavar='number_slices', type=int,
                    default=112, help='number of slices to use')

parser.add_argument('--input_chanels', metavar='input_chanels', type=int,
                    default=3, help='do side information lesion')
# Misc
parser.add_argument('--seed', type=int, default=42, metavar='seed',
                    help='random seed (default: 42)')

parser.add_argument('--workers', type=int, default=2, metavar='workers',
                    help='how many training processes to use (default: 4)')

parser.add_argument('--device', default='cuda',
                    help='device to use for training / testing')

parser.add_argument('--stage', metavar='stage', type=str,
                    default='test',
                    help='Define the stage')

parser.add_argument('--cls_problem', metavar='cls_problem', type=str, choices=['covid_cls', 'covid_severity_cls'],
                    default='covid_severity_cls', help='This is to control for each competition')
@torch.no_grad()
def evaluate_model_val(args, epoch, model, val_loader, criterion,  cut_off=None):#=0.745380
    device = args.device

    val_loss = 0

    correct_pred = 0

    num_examples = 0

    with torch.no_grad():

        model.eval()

        for batch_idx, batch_data in enumerate(val_loader):

            inputs, targets = batch_data['image_ct'].to(device), batch_data['label'].to(device)

            inputs = repeat(inputs, 'b c h w d ->b (repeat c) h w d ',
                            repeat=3)

            inputs = rearrange(inputs, 'b c h w d -> b c d h w')

            outputs = model(inputs)

            batch_loss = criterion(outputs, targets.long())

            val_loss += batch_loss

            probas = F.softmax(outputs, dim=1)


            if cut_off is not None and args.cls_problem=='cls_problem': ###thi is not optimal at all... but is late
                predicted_labels=torch.from_numpy(np.where(probas.cpu().numpy()[:,1]>=cut_off,1,0)).to(device)
            else:
                predicted_labels = probas.argmax(dim=1)

            num_examples += len(targets)
            correct_pred += torch.eq(predicted_labels, targets).sum().item()

            if batch_idx == 0:
                scores = probas.detach().cpu().numpy()
                labels = targets.detach().cpu().numpy()
            else:
                scores = np.concatenate((scores, probas.detach().cpu().numpy()), axis=0)
                labels = np.concatenate((labels, targets.detach().cpu().numpy()))

        metric_collects = utils.calc_multi_cls_measures_ECCV2022_Challenge(args.cls_problem,scores, labels,cut_off=cut_off)
        loss = val_loss / len(val_loader)
        utils.print_epoch_metrics_ECCV2022_Challenge(args, epoch=epoch, metric_collects=metric_collects,
                                  stage='Evaluation',loss=loss)
    return metric_collects, loss , scores, labels

@torch.no_grad()
def evaluate_model(args, model, val_loader,  cut_off=None):
    device = args.device

    with torch.no_grad():

        model.eval()

        for batch_idx, batch_data in enumerate(val_loader):

            inputs = batch_data['image_ct'].to(device)

            inputs = repeat(inputs, 'b c h w d ->b (repeat c) h w d ',
                            repeat=3)
            # print(inputs.shape)
            inputs = rearrange(inputs, 'b c h w d -> b c d h w')

            outputs = model(inputs)

            probas = F.softmax(outputs, dim=1)

            if batch_idx == 0:
                scores = probas.detach().cpu().numpy()
            else:
                scores = np.concatenate((scores, probas.detach().cpu().numpy()), axis=0)

    return scores

def main(args):

    if not args.model:
        print("Nothing to do")
        sys.exit()
    else:

        seed = args.seed + utils.get_rank()
        torch.manual_seed(seed)
        np.random.seed(seed)
        cudnn.benchmark = True

        ### If args.checkpoint_root does not exists ... error
        if not os.path.exists(args.checkpoint_output):
            raise ValueError(
                "The specified checkpoint output directory {} does not exits...\n".format(args.checkpoint_output))
        try:
            model = create_model(
                args.model,
                pretrained=False,
                in_22k=False,
                num_classes=args.num_classes,
                drop_path_rate=0.0,
                layer_scale_init_value=args.layer_scale_init_value,
                head_init_scale=args.head_init_scale,
            )

            # Device
            args.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
            device = torch.device(args.device)

            checkpoint = torch.load(os.path.join(args.checkpoint_output, 'model_val_epoch_{}.pt'.format(args.epoch)),
                                    map_location=device)

            if args.model_ema:
                model.load_state_dict(checkpoint['ema_state_state'],strict=False)
            else:
                model.load_state_dict(checkpoint['state'], strict=False)

            model.to(device)

            print('Starting evaluation | model: {}...\n'.format(args.model))

            if args.label_smoothing > 0.:
                criterion = LabelSmoothingCrossEntropy(smoothing=args.label_smoothing)
            else:
                criterion = torch.nn.CrossEntropyLoss()
            if args.cls_problem == 'covid_cls':

                if args.stage == 'val':
                    val_ds, val_loader, files = utils.get_dataset_and_dataloader_ECCV_Challenge2022_Eval(
                        os.path.join(args.input_data,
                                     'data_{}_files_v_db_ECCV_2022.csv'.format(
                                         'val')),
                        stage='val',
                        cls_problem=args.cls_problem,
                        do_augment=False,
                        batch_size=args.eval_batchsize,
                        shuffle=False,
                        num_classes=args.num_classes,
                        num_workers=args.workers,
                        drop_last=False,
                        do_weigthed_sampler=False,
                        read_util=None,
                        return_lung_masks=False,
                        return_lesions_masks=False,
                        ratio_unlabeled=None,
                        return_labels=True,
                        )

                    metrics_collects_eval, loss, prediction_prob, labels = evaluate_model_val(args, epoch=args.epoch, model=model, val_loader=val_loader, criterion=criterion)

                    print('Saving pred_labels data...\n')

                    fieldnames_writer = ['idx', 'scan_id', 'numerical_values', 'true_class_labels', 'labels']

                    with open(os.path.join(args.checkpoint_output, '{}_out_pred_prob.csv'.format(args.stage)), 'w',
                              newline='') as csvfilew:

                        writer = csv.DictWriter(csvfilew, fieldnames=fieldnames_writer)

                        writer.writeheader()

                        for idx in range(len(labels)):
                            if labels[idx] == 1:
                                true_class_labels = True
                            else:
                                true_class_labels = False

                            temp_name = files[idx]
                            temp_name['image_ct'].split('/')[-2]
                            writer.writerow({'idx': int(idx),
                                             'scan_id': temp_name['image_ct'].split('/')[-2],
                                             'numerical_values': '%.8f' % prediction_prob[idx, 1],
                                             ###take prob of the positive class
                                             'true_class_labels': true_class_labels,
                                             'labels': labels[idx]})

                else:
                    val_ds, val_loader, files = utils.get_dataset_and_dataloader_ECCV_Challenge2022_Eval(
                        os.path.join(args.input_data,
                                     'data_{}_files_v_db_ECCV_2022.csv'.format(
                                         'test')),
                        stage='val',
                        cls_problem=args.cls_problem,
                        do_augment=False,
                        batch_size=args.eval_batchsize,
                        shuffle=False,
                        num_classes=args.num_classes,
                        num_workers=args.workers,
                        drop_last=False,
                        do_weigthed_sampler=False,
                        read_util=None,
                        return_lung_masks=False,
                        return_lesions_masks=False,
                        ratio_unlabeled=None,
                        return_labels=False,
                    )
                    prediction_prob = evaluate_model(args, model=model, val_loader=val_loader)

                    print('Saving pred_labels data...\n')

                    fieldnames_writer = ['idx', 'scan_id', 'numerical_values', 'true_class_labels', 'labels']

                    with open(os.path.join(args.checkpoint_output, '{}_{}_out_pred_prob.csv'.format('test',args.cls_problem)), 'w',
                              newline='') as csvfilew:

                        writer = csv.DictWriter(csvfilew, fieldnames=fieldnames_writer)

                        writer.writeheader()

                        for idx in range(len(files)):
                            temp_name = files[idx]
                            temp_name['image_ct'].split('/')[-2]
                            writer.writerow({'idx': int(idx),
                                             'scan_id': temp_name['image_ct'].split('/')[-2],
                                             'numerical_values': '%.8f' % prediction_prob[idx, 1],
                                             ###take prob of the positive class
                                             'true_class_labels': -1,
                                             'labels': -1})


                print('Saving pred_probabilities data...\n')

            elif args.cls_problem == 'covid_severity_cls':

                if args.stage == 'val':
                    val_ds, val_loader, files = utils.get_dataset_and_dataloader_ECCV_Challenge2022_Eval(
                        os.path.join(args.input_data,
                                     'data_{}_files_v_db_ECCV_2022.csv'.format(
                                         'val')),
                        stage='val',
                        cls_problem=args.cls_problem,
                        do_augment=False,
                        batch_size=args.eval_batchsize,
                        shuffle=False,
                        num_classes=args.num_classes,
                        num_workers=args.workers,
                        drop_last=False,
                        read_util=None,
                        return_lung_masks=False,
                        return_lesions_masks=False,
                        ratio_unlabeled=None,
                        return_labels=True,
                    )

                    metrics_collects_eval, loss, prediction_prob, labels = evaluate_model_val(args, epoch=args.epoch,
                                                                                              model=model,
                                                                                              val_loader=val_loader,
                                                                                              criterion=criterion)

                    print('Saving pred_labels data...\n')

                    fieldnames_writer = ['idx', 'scan_id', 'numerical_values', 'pred', 'labels']

                    with open(os.path.join(args.checkpoint_output, '{}_{}_out_pred_prob.csv'.format(args.stage, args.cls_problem)), 'w',
                              newline='') as csvfilew:

                        writer = csv.DictWriter(csvfilew, fieldnames=fieldnames_writer)

                        writer.writeheader()

                        for idx in range(len(labels)):
                            temp_name = files[idx]
                            temp_name['image_ct'].split('/')[-2]
                            writer.writerow({'idx': int(idx),
                                             'scan_id': temp_name['image_ct'].split('/')[-2],
                                             'numerical_values': '%.8f' % np.max(prediction_prob[idx,...]),
                                             'pred': np.argmax(prediction_prob[idx,...]),
                                             ###take prob of the positive class
                                             'labels': labels[idx]})

                else:
                    val_ds, val_loader, files = utils.get_dataset_and_dataloader_ECCV_Challenge2022_Eval(
                        os.path.join(args.input_data,
                                     'data_{}_files_v_db_ECCV_2022.csv'.format(
                                         'test_severity')),
                        stage='test',
                        cls_problem=args.cls_problem,
                        do_augment=False,
                        batch_size=args.eval_batchsize,
                        shuffle=False,
                        num_classes=args.num_classes,
                        num_workers=args.workers,
                        drop_last=False,
                        do_weigthed_sampler=False,
                        read_util=None,
                        return_lung_masks=False,
                        return_lesions_masks=False,
                        ratio_unlabeled=None,
                        return_labels=False,
                    )

                    prediction_prob = evaluate_model(args, model=model, val_loader=val_loader)

                    print('Saving pred_labels data...\n')

                    fieldnames_writer = ['idx', 'scan_id', 'numerical_values', 'pred', 'labels']

                    with open(os.path.join(args.checkpoint_output,
                                           '{}_{}_out_pred_prob.csv'.format('test', args.cls_problem)), 'w',
                              newline='') as csvfilew:

                        writer = csv.DictWriter(csvfilew, fieldnames=fieldnames_writer)

                        writer.writeheader()

                        for idx in range(len(files)):
                            temp_name = files[idx]
                            temp_name['image_ct'].split('/')[-2]
                            writer.writerow({'idx': int(idx),
                                             'scan_id': temp_name['image_ct'].split('/')[-2],
                                             'numerical_values': '%.8f' % np.max(prediction_prob[idx, ...]),
                                             'pred': '%.8f' % np.argmax(prediction_prob[idx,...]),
                                             'labels' : -1,
                                             })

                print('Saving pred_probabilities data...\n')

        except Exception as e:

            print("The following error occurred: {} \n".format(str(e)))


if __name__ == '__main__':
    argsin = sys.argv[1:]
    args = parser.parse_args(argsin)
    print(args.__dict__)
    main(args)
    sys.exit()

