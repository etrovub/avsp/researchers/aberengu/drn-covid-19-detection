##########################################################################################
# (c) Copyright 2022
# The author(s):    Abel Díaz Berenguer
#                   Tanmoy Mukherjee
# Vrije Universiteit Brussel,
# Department of Electronics and Informatics (ETRO),
# Faculty of Engineering Sciences,
# VUB-ETRO,
# 2 Pleinlaan, 1050 Belgium.
# (VUB - ETRO)
#
# All right reserved
# Permission to use, copy, modify, and distribute within VUB-ETRO
# this routine/program/software and/or its documentation, or part of any one of them
# (the program) for research purpose within VUB-ETRO is hereby granted, provided:
# (1) that the copyright notice appears in all copies,
# (2) that both the copyright notice and this permission notice appear in the supporting documentation, and
# (3) that all publications based partially or completely on this program will have the main publications of "the authors" related to this
# work cited as references.
# Any other type of handling or use of the program, CAN NOT BE DONE
# WITHOUT PRIOR WRITTEN APPROVAL from the VUB-ETRO,
# including but not restricted to the following:
# (A) any re-distribution of the program to any person outside of the VUB-ETRO
# (B) any use of the program which is not academic research;
# (C) any commercial activity involving the use of the program is strictly prohibited.
# Modifications: 2018
# Disclaimer: VUB ETRO make no claim about the
# suitability of the program for any particular purpose.
# It is provided "as is" without any form of warranty or support.
##########################################################################################
import argparse
import numpy as np
import torch.nn.functional as F
import torch.nn as nn
import torch
import torchvision
import time
import os
import errno
import sys
import csv

from datetime import datetime
from utils import get_dataset_and_dataloader_ECCV_Challenge2022
from train_v5_3_MT_ECCV2022_Challenge import train_model
from model.convnext3D_MT import convnext_large as convnext

import yaml
import os
import logging
from collections import OrderedDict
from contextlib import suppress
from datetime import datetime
import pandas as pd

from timm.data import create_dataset, create_loader, resolve_data_config, Mixup, FastCollateMixup, AugMixDataset
from timm.models import create_model, safe_model_name, resume_checkpoint, load_checkpoint, \
    convert_splitbn_model, model_parameters
from timm.utils import *
from timm.loss import *
from timm.optim import create_optimizer_v2, optimizer_kwargs
from timm.scheduler import create_scheduler
from timm.utils import ApexScaler, NativeScaler
import utils

from optim_factory import create_optimizer, LayerDecayValueAssigner
import torch.backends.cudnn as cudnn

try:
    from apex import amp
    from apex.parallel import DistributedDataParallel as ApexDDP
    from apex.parallel import convert_syncbn_model

    has_apex = True
except ImportError:
    has_apex = False

has_native_amp = False
try:
    if getattr(torch.cuda.amp, 'autocast') is not None:
        has_native_amp = True
except AttributeError:
    pass

try:
    import wandb

    has_wandb = True
except ImportError:
    has_wandb = False

# torch.backends.cudnn.benchmark = True
# _logger = logging.getLogger('train')



def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

##########################
### SETTINGS
##########################
RANDOM_SEED = 42
# The first arg parser parses out only the --config argument, this argument is used to
# load a yaml file containing key-values that override the defaults for the main parser below
config_parser = parser = argparse.ArgumentParser(description='Training Config', add_help=False)
##########################
### Data
##########################
parser.add_argument('--input_data', metavar='input_data', type=str,
                    default='/home/abel/Project/Data/ECCV2022_CHALLENGE_MIA-19-DB/',
                    help='Path to the root folder with the training data')
parser.add_argument('--is_training', metavar='is_training', type=str2bool,
                    default=True,
                    help='Define if the model is in training or evaluations mode')
# * Finetuning params
parser.add_argument('--finetune', default='',
                    help='finetune from checkpoint')
parser.add_argument('--head_init_scale', default=1.0, type=float,
                    help='classifier head initial scale, typically adjusted in fine-tuning')
parser.add_argument('--model_key', default='model|module', type=str,
                    help='which key to load from saved state dict, usually model or model_ema')
parser.add_argument('--model_prefix', default='', type=str)

##########################
### Data preprocessing
##########################
parser.add_argument('--do_crop', metavar='is_training', type=str2bool,
                    default=False,
                    help='Define if crop or mask')

parser.add_argument('--lower_HU', metavar='is_training', type=int,
                    default=-1150,
                    help='Define if crop or mask')

parser.add_argument('--higher_HU', metavar='is_training', type=int,
                    default=350,
                    help='Define if crop or mask')

parser.add_argument('--do_data_augmentation', metavar='do_data_augmentation', type=str2bool,
                    default=True, help='To do data augmentation or not')  ###Best resutls with this value in 1e-5
##########################
### Model Optimization
##########################

# Optimizer parameters
parser.add_argument('--opt', default='adamw', type=str, metavar='optimizer',
                    choices=['adamw', 'sgd', 'lars', 'lamb'], help='Define the type of optimizer. Recommend using adamw with ViTs.')
parser.add_argument('--opt-eps', default=None, type=float, metavar='optimizer_eps',
                    help='Optimizer Epsilon (default: None, use opt default)')
parser.add_argument('--opt-betas', default=None, type=float, nargs='+', metavar='optimizer_betas',
                    help='Optimizer Betas (default: None, use opt default)')
parser.add_argument('--momentum', type=float, default=0.9, metavar='optimizer_momentum',
                    help='Optimizer momentum (default: 0.9)')
parser.add_argument('--weight-decay', type=float, default=2e-5,
                    help='weight decay (default: 2e-5)')
parser.add_argument('--weight-decay-end', metavar='weight-decay', default=2e-2, type=float)
parser.add_argument('--clip-grad', type=float, default=None, metavar='NORM',
                    help='Clip gradient norm (default: None, no clipping)')
parser.add_argument('--clip-mode', type=str, default='norm',
                    help='Gradient clipping mode. One of ("norm", "value", "agc")')

# Learning rate schedule parameters

parser.add_argument('--lr', type=float, default=4e-5, metavar='LR',
                    help='learning rate (default: 0.05)')

parser.add_argument('--min-lr', type=float, default=1e-6, metavar='LR',
                    help='lower lr bound for cyclic schedulers that hit 0 (1e-6)')

parser.add_argument('--layer_decay', type=float, default=1.0)

parser.add_argument('--warmup_steps', type=int, default=-1, metavar='N',
                        help='num of steps to warmup LR, will overload warmup_epochs if set > 0')

parser.add_argument('--warmup-epochs', type=int, default=20, metavar='N',
                    help='epochs to warmup LR, if scheduler supports')

# Model Exponential Moving Average
parser.add_argument('--do_gn_mt', type=str2bool, default=False,
                    help='train with MT')

parser.add_argument('--model-ema', type=str2bool, default=True,
                    help='Enable tracking moving average of model weights')

parser.add_argument('--model-ema-force-cpu', type=str2bool, default=False,
                    help='Force ema to be tracked on CPU, rank=0 node only. Disables EMA validation.')

parser.add_argument('--model-ema-decay', type=float, default=0.9998,
                    help='decay factor for model weights moving average (default: 0.9998)')

parser.add_argument('--ratio_unlabeled', metavar='ratio_unlabeled', type=float,
                    default=200, help='Define the ration of unlabeled data from the extra dataset')

parser.add_argument('--semi_supervised', metavar='semi_supervised', type=str2bool,
                    default=False, help='This is to train with less data')

parser.add_argument('--second_dataset', metavar='second_dataset', type=str,
                    default=None)

parser.add_argument('--cls_problem', metavar='cls_problem', type=str, choices=['covid_cls', 'covid_severity_cls'],
                    default='covid_severity_cls', help='This is to control for each competition')

parser.add_argument('--consistency_type', metavar='consistency_type', type=str, choices=['mse', 'kl'],
                    default='mse', help='define the consistency type for the MT')

parser.add_argument('--consistency_weight', default=0.3, type=float, metavar='consistency_weight',
                    help='use consistance loss')#--> _lambda

parser.add_argument('--logit_distance_weight', default=-1, type=float, metavar='logit_distance_weight',
                    help='let use a residual loss between logits and softmax output)')

parser.add_argument('--consistency_weight_rampup', default=30, type=int, metavar='consistency_weight_rampup',
                    help='length of the consistency loss ramp-up') ##should we use the same epochs and values --> _lambda

parser.add_argument('--proj_distance_type', metavar='consistency_type', type=str, choices=['mse', 'kl','smi','infomax','jsd','smis'],
                    default='smi', help='define the contrastive distance type')###add other choices

parser.add_argument('--proj_distance_weight', default=0.3, type=float, metavar='proj_distance_weight',
                    help='let the student model have two outputs and use a loss between the proj with the given weight (default: only have one output)') #--> _beta

parser.add_argument('--distance_weight_rampup', default=30, type=int, metavar='distance_weight_rampup',
                    help='length of the distance_cost loss ramp-up') ##should we use the same epochs and values --> beta

# Regularization parameters
parser.add_argument('--drop_path_rate', type=float, default=0.4, help="Stochastic depth rate")#0.3

parser.add_argument('--checkpoint_output', metavar='checkpoint_output', type=str,
                    default='/home/abel/Project/EXP_COVID-19_V5_3/ECCV_Challenge/June2022/Logs_and_models/',
                    help='Path to the root folder to save the checkpoint')

parser.add_argument('--label_smoothing', metavar='label_smoothing', type=float,
                    default=0.4,
                    help='Label_smoothing fro CE loss')


# Misc training options

parser.add_argument('--batchsize', type=int, default=4, metavar='batchsize',
                    help="Number of volumes to process simultaneously. Lower number requires less memory but may be slower")

parser.add_argument('--validation_batchsize', type=int, default=1, metavar='validation_batchsize',
                    help='validation batch size override (default: 1)')

parser.add_argument('--check_every', type=int, default=1,
                    help="amount of epoch to perform validation")

parser.add_argument('--num_epochs', metavar='epochs', type=int,
                    default=300,
                    help='Define if the number of epochs to train')

parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
                    help='manual epoch number (useful on restarts)')

parser.add_argument('--update_freq', default=1, type=int,
                        help='gradient accumulation steps')

parser.add_argument('--num_epochs_val_early_stop', metavar='num_epochs_val_early_stop', type=int,
                    default=100, help='Number of epochs to stop if non improvements regarding the validation loss')

##########################
### Pre-trained Model
##########################
parser.add_argument('--pretrained', metavar='pretrained', type=str,
                    choices=['ImageNet', 'STOIC','Scratch'],
                    default='ImageNet',
                    help='Optimize pretrained model')
##########################
### Model Parameters
##########################
parser.add_argument('--model', type=str, choices=['convnext_xlarge', 'convnext_large', 'convnext_base',  'convnext_small','convnext_tiny'],
                    help='the model to use',
                    default='convnext_base')#So far 3 models

parser.add_argument('--layer_scale_init_value', default=1e-6, type=float,
                        help="Layer scale initial values")

parser.add_argument('--num_classes', type=int,
                    help="Number of classes",
                    default=4)

parser.add_argument('--in_plane_x', metavar='in_plane_size', type=int,
                    default=224, help='in_plane_size_x')

parser.add_argument('--in_plane_y', metavar='in_plane_size', type=int,
                    default=224, help='in_plane_size_y')

parser.add_argument('--number_slices', metavar='number_slices', type=int,
                    default=112, help='number of slices to use')

parser.add_argument('--input_chanels', metavar='input_chanels', type=int,
                    default=3, help='number of input_chanels')

######this are arguments from the train.py of timm
# Misc
parser.add_argument('--seed', type=int, default=42, metavar='seed',
                    help='random seed (default: 42)')

parser.add_argument('--workers', type=int, default=4, metavar='workers',
                    help='how many training processes to use (default: 4)')

parser.add_argument('--device', default='cuda',
                        help='device to use for training / testing')

def main(args):

    if not args.model:
        print("Nothing to do")
        sys.exit()
    else:

        if args.semi_supervised and args.consistency_weight < 0:
            raise ValueError(
                "Can not train semisupervised if consistency_weight is negative. The consistency_weight value is :{}...\n".format(args.consistency_weight))

        if args.cls_problem=='covid_severity_cls':
            if args.num_classes <= 2:
                raise ValueError(
                    "This problem : {} requires more than {} classes...\n".format(
                        args.cls_problem, args.num_classes))
        elif args.cls_problem=='covid_cls':
            if args.num_classes > 2:
                    raise ValueError(
                        "This problem : {} requires less than {} classes...\n".format(
                            args.cls_problem, args.num_classes))

        # fix the seed for reproducibility
        seed = args.seed + utils.get_rank()
        torch.manual_seed(seed)
        np.random.seed(seed)
        cudnn.benchmark = True

        # ##This is OK_Uncoment to make it work
        train_ds, train_loader = get_dataset_and_dataloader_ECCV_Challenge2022(
            os.path.join(args.input_data,
                         'data_{}_files_v_db_ECCV_2022.csv'.format(
                             'train')),
            stage='train',
            cls_problem=args.cls_problem,
            do_augment=args.do_data_augmentation,
            batch_size=args.batchsize,
            num_classes = args.num_classes,
            shuffle=True,
            num_workers=args.workers,
            drop_last=True,
            do_weigthed_sampler=False,  ### if true shfule is False, priority is given to this parameter TODO must be en argumet
            read_util=10,  ### Uses only 50 data points
            return_lung_masks=False,
            return_lesions_masks=False,
            ratio_unlabeled = args.ratio_unlabeled if args.ratio_unlabeled > 0.0 else None,
            semi_supervised=args.semi_supervised,
            second_dataset=args.second_dataset,
            return_labels=True,
        )

        val_ds, val_loader = get_dataset_and_dataloader_ECCV_Challenge2022(os.path.join(args.input_data,
                                                                     'data_{}_files_v_db_ECCV_2022.csv'.format('val')),
                                                        stage='val',
                                                        cls_problem=args.cls_problem,
                                                        do_augment=False,
                                                        batch_size=args.validation_batchsize,
                                                        shuffle=False,
                                                        num_classes=args.num_classes,
                                                        num_workers=args.workers,
                                                        drop_last=False,
                                                        do_weigthed_sampler=False,### if true shfule is False, priority is given to this parameter TODO must be en argumet
                                                        read_util=10,  ##do read the full list
                                                        return_lung_masks=False,
                                                        return_lesions_masks=False,
                                                        ratio_unlabeled = None,
                                                        return_labels=True,
                                                        )

        args.checkpoint_output=args.checkpoint_output if args.checkpoint_output is not None else './output/train'

        root_folder = os.path.join(args.checkpoint_output, args.model)

        date_time = datetime.now().strftime("exp_date_%m_%d_%Y_time_%H_%M_%S")

        args.checkpoint_output = os.path.join(root_folder,'{}/model_pretrained_{}_do_Augment_{}_{}'.format(str(args.cls_problem),
            str(args.pretrained), str(args.do_data_augmentation),date_time))
        ### If args.checkpoint_root does not exists create the folder...
        if not os.path.exists(args.checkpoint_output):
            try:
                os.makedirs(args.checkpoint_output)
            except OSError as exception:
                if exception.errno == errno.EEXIST and os.path.isdir(args.checkpoint_output):
                    pass
                else:
                    raise ValueError(
                        "Failed to created output directory {}...\n".format(args.checkpoint_output))

        ####save args_dict
        # define a dictionary with key value pairs
        with open(os.path.join(args.checkpoint_output,"args_input_conf.csv"), 'w', newline='') as csvfile:
            w = csv.writer(csvfile)
            for key, val in vars(args).items():
                # write every key and value to file
                w.writerow([key, val])


        model = create_model(
            args.model,
            pretrained=True,
            in_22k=True,
            proj_dim=256 if args.proj_distance_weight >= 0 else None,
            num_classes=args.num_classes,
            drop_path_rate=args.drop_path_rate,
            layer_scale_init_value=args.layer_scale_init_value,
            head_init_scale=args.head_init_scale,
        )

        if torch.cuda.is_available():
            torch.backends.cudnn.deterministic = True
        #Device
        args.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        device = torch.device(args.device)


        checkpoint = torch.load(os.path.join(args.input_data,'model_val_epoch_52.pt'),
                                map_location=device)

        ####use the model state instead of ema state
        if args.cls_problem=='covid_severity_cls': #Skip the last layer
            pretrained_dict = checkpoint['ema_state_state']            # 1. filter out unnecessary keys
            #
            #model_dict = model.state_dict()

            pretrained_dict = {k: v for k, v in pretrained_dict.items() if (k!='head.weight' and k!='head.bias')}
            # if k in model_dict
            # 2. overwrite entries in the existing state dict
            #model_dict.update(pretrained_dict)
            # 3. load the new state dict
            model.load_state_dict(pretrained_dict, strict=False)

        else:
            checkpoint = torch.load(os.path.join(args.input_data, 'model_val_epoch_52.pt'),
                                    map_location=device)
            model.load_state_dict(checkpoint['ema_state_state'], strict=False)

        model.to(device)

        print('Starting training | model: {}...\n'.format(args.model))

        print('Here is the model:')
        print(model)

        model_ema = None
        if args.model_ema:
            # Important to create EMA model after cuda(), DP wrapper, and AMP but before SyncBN and DDP wrapper
            model_ema = ModelEmaV2(
                model,
                decay=args.model_ema_decay,
                device='cpu' if args.model_ema_force_cpu else device)
            print("Using EMA with decay = %.8f" % args.model_ema_decay)

        # model_without_ddp = model
        n_parameters = sum(p.numel() for p in model.parameters() if p.requires_grad)

        print("Model = %s" % str(model))
        print('number of params:', n_parameters)

        total_batch_size = args.batchsize * args.update_freq * utils.get_world_size()
        num_training_steps_per_epoch = len(train_ds) // total_batch_size
        print("LR = %.8f" % args.lr)
        print("Batch size = %d" % total_batch_size)
        print("Update frequent = %d" % args.update_freq)
        print("Number of training examples = %d" % len(train_ds))
        print("Number of training training per epoch = %d" % num_training_steps_per_epoch)

        if args.layer_decay < 1.0 or args.layer_decay > 1.0:
            num_layers = 12  # convnext layers divided into 12 parts, each with a different decayed lr value.
            assert args.model in ['convnext_small', 'convnext_base', 'convnext_large', 'convnext_xlarge'], \
                "Layer Decay impl only supports convnext_small/base/large/xlarge"
            assigner = LayerDecayValueAssigner(
                list(args.layer_decay ** (num_layers + 1 - i) for i in range(num_layers + 2)))
        else:
            assigner = None

        if assigner is not None:
            print("Assigned values = %s" % str(assigner.values))


        optimizer = create_optimizer(
            args, model, skip_list=None,
            get_num_layer=assigner.get_layer_id if assigner is not None else None,
            get_layer_scale=assigner.get_scale if assigner is not None else None)

        ###NO_LABEL=-1
        if args.label_smoothing > 0. and not args.semi_supervised:
            criterion = LabelSmoothingCrossEntropy(smoothing=args.label_smoothing)
        elif args.label_smoothing > 0. and args.semi_supervised:
            criterion = nn.CrossEntropyLoss(label_smoothing=args.label_smoothing,ignore_index=-1)
        elif args.label_smoothing <= 0. and args.semi_supervised:
            criterion = nn.CrossEntropyLoss(ignore_index=-1)
        else:
            criterion = nn.CrossEntropyLoss()

        print("criterion = %s" % str(criterion))

        train_model(args,model, optimizer,train_loader=train_loader, val_loader=val_loader,
                    num_training_steps_per_epoch=num_training_steps_per_epoch, criterion=criterion, model_ema=model_ema)

        #except Exception as e:

        #     print("The following error occurred: {} \n".format(str(e)))

if __name__ == '__main__':
    argsin = sys.argv[1:]
    args = parser.parse_args(argsin)
    print(args.__dict__)
    main(args)
    sys.exit()  # 3212450842

