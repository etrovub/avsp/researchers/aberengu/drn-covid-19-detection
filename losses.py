# Copyright (c) 2018, Curious AI Ltd. All rights reserved.
#
# This work is licensed under the Creative Commons Attribution-NonCommercial
# 4.0 International License. To view a copy of this license, visit
# http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to
# Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

"""Custom loss functions"""

import torch
from torch.nn import functional as F

def softmax_mse_loss(input_logits, target_logits):
    """Takes softmax on both sides and returns MSE loss
    Note:
    - Returns the sum over all examples. Divide by the batch size afterwards
      if you want the mean.
    - Sends gradients to inputs but not the targets.
    """
    assert input_logits.size() == target_logits.size()
    input_softmax = F.softmax(input_logits, dim=1)
    target_softmax = F.softmax(target_logits, dim=1)
    num_classes = input_logits.size()[1]
    return F.mse_loss(input_softmax, target_softmax, size_average=False) / num_classes


def softmax_kl_loss(input_logits, target_logits):
    """Takes softmax on both sides and returns KL divergence
    Note:
    - Returns the sum over all examples. Divide by the batch size afterwards
      if you want the mean.
    - Sends gradients to inputs but not the targets.
    """
    assert input_logits.size() == target_logits.size()
    input_log_softmax = F.log_softmax(input_logits, dim=1)
    target_softmax = F.softmax(target_logits, dim=1)
    return F.kl_div(input_log_softmax, target_softmax, size_average=False)


def softmax_JSD_loss(p, q):
    """Takes softmax on both sides and returns KL divergence
    Note:
    - Returns the sum over all examples. Divide by the batch size afterwards
      if you want the mean.
    - Sends gradients to inputs but not the targets. ###second is ema_student
    """
    assert p.size() == q.size()
    p, q = p.view(-1, p.size(-1)), q.view(-1, q.size(-1))
    m = (0.5 * (p + q)).log()
    return 0.5 * (F.kl_div(m, p.log(),log_target=True) + F.kl_div(m, q.log(),log_target=True))

    # input_log_softmax = F.log_softmax(input_logits, dim=1)
    # target_softmax = F.softmax(target_logits, dim=1)
    # M = 0.5 * (input_log_softmax + target_softmax)
    # return 0.5 * ( F.kl_div(input_log_softmax, M, size_average=False) + F.kl_div(target_softmax, M, size_average=False))


def proj_loss(input1, input2): ###thi is from Byol
    assert input1.size() == input2.size()
    input1 = F.normalize(input1, dim=-1, p=2)
    input2 = F.normalize(input2, dim=-1, p=2)
    return torch.sum(2 - 2 * (input1 * input2)) ###2 - 2 * (input1 * input2).sum(dim=-1) ###recheck this one

def symmetric_mse_loss(input1, input2):
    """Like F.mse_loss but sends gradients to both directions
    Note:
    - Returns the sum over all examples. Divide by the batch size afterwards
      if you want the mean.
    - Sends gradients to both input1 and input2.
    """
    assert input1.size() == input2.size()
    num_classes = input1.size()[1]
    return torch.sum((input1 - input2)**2) / num_classes


def poly_kernel(f_X, d=0.5, alpha=1.0, c=2.0):
    K_XX = torch.mm(f_X, f_X.t()) + c
    K_XX = torch.abs(K_XX) ###for the Eccv version this was not needed
    return K_XX.pow(d)

def smi(f_X, f_Y,device='cuda'):
    assert f_X.size() == f_Y.size()
    K_X = poly_kernel(f_X)
    K_Y = poly_kernel(f_Y)
    n = K_X.size(0)
    phi = K_X * K_Y
    hh = torch.mean(phi, 1)
    Hh = K_X.mm(K_X.t()) * K_Y.mm(K_Y.t()) / (n ** 2 + torch.eye(n,device=device))
    alphah = torch.matmul(torch.inverse(Hh), hh)

    smi = 0.5 * torch.dot(alphah, hh) - 0.5
    return smi#, alphah


def smisoft(f_X, f_Y,device='cuda'):
    assert f_X.size() == f_Y.size()
    # f_X = F.normalize(f_X, dim=-1, p=2)
    # f_Y = F.normalize(f_Y, dim=-1, p=2)
    f_X = F.softmax(f_X, dim=1)
    f_Y = F.softmax(f_Y, dim=1)
    K_X = poly_kernel(f_X)
    K_Y = poly_kernel(f_Y)
    n = K_X.size(0)
    phi = K_X * K_Y
    hh = torch.mean(phi, 1)
    Hh = K_X.mm(K_X.t()) * K_Y.mm(K_Y.t()) / (n ** 2 + torch.eye(n,device=device))
    alphah = torch.matmul(torch.inverse(Hh), hh)

    smi = 0.5 * torch.dot(alphah, hh) - 0.5
    return smi#, alphah

def marginalPdf(values,num_bins=2048,sigma=0.4,epsilon = 1e-10,device='cuda'):###todo the number of bins is the dim_proj d_
    sigma = 2 * sigma ** 2
    bins = torch.nn.Parameter(torch.linspace(0, num_bins-1, num_bins, device=device).float(), requires_grad=False)
    residuals = values - bins.unsqueeze(0).unsqueeze(0)
    kernel_values = torch.exp(-0.5 * (residuals / sigma).pow(2))

    pdf = torch.mean(kernel_values, dim=1)
    normalization = torch.sum(pdf, dim=1).unsqueeze(1) + epsilon
    pdf = pdf / normalization

    return pdf, kernel_values

def jointPdf(kernel_values1, kernel_values2,epsilon = 1e-10):
    joint_kernel_values = torch.matmul(kernel_values1.transpose(1, 2), kernel_values2)
    normalization = torch.sum(joint_kernel_values, dim=(1, 2)).view(-1, 1, 1) + epsilon
    pdf = joint_kernel_values / normalization

    return pdf

def getMutualInformation(input1, input2,normalize=True, epsilon = 1e-10):
    '''
        input1: B \times proj_dim
        input2: B, \times proj_dim

        return: scalar
    '''

    # # Torch tensors for images between (0, 1)
    # input1 = input1 * 255
    # input2 = input2 * 255

    # B, feat = input1.shape
    assert ((input1.shape == input2.shape))

    # x1 = input1.view(B, H * W, C)
    # x2 = input2.view(B, H * W, C)

    pdf_x1, kernel_values1 = marginalPdf(input1)
    pdf_x2, kernel_values2 = marginalPdf(input1)
    pdf_x1x2 = jointPdf(kernel_values1, kernel_values2)

    H_x1 = -torch.sum(pdf_x1 * torch.log2(pdf_x1 + epsilon), dim=1)
    H_x2 = -torch.sum(pdf_x2 * torch.log2(pdf_x2 + epsilon), dim=1)
    H_x1x2 = -torch.sum(pdf_x1x2 * torch.log2(pdf_x1x2 + epsilon), dim=(1, 2))

    mutual_information = H_x1 + H_x2 - H_x1x2

    if normalize:
        mutual_information = 2 * mutual_information / (H_x1 + H_x2)

    return -mutual_information