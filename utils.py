import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import f1_score
from sklearn.metrics import average_precision_score
from sklearn.metrics import PrecisionRecallDisplay
from sklearn.metrics import precision_recall_curve
import os
from matplotlib import pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split

from torch.utils.data import DataLoader
import torch
import monai
from monai.data import CacheDataset
from monai.transforms import  LoadImaged, \
    ScaleIntensityRanged, Rotate90d, CastToTyped, ToTensord, \
    AddChanneld, ResizeWithPadOrCropd, Orientationd, MaskIntensityd, DeleteItemsd, RandAffined, \
    RandGaussianSmoothd, RandHistogramShiftd, RandZoomd, RandGaussianNoised, RandFlipd, \
    RandAdjustContrastd, CropForegroundd, Flipd, RandShiftIntensityd, RandRotate90d, EnsureTyped

##########################
### Data handling
##########################
def make_weights_for_balanced_classes(ct_instances, num_classes):
    count = [0] * num_classes
    for ct in ct_instances:
        count[int(ct['label'])] += 1
    weight_per_class = [0.] * num_classes
    N = float(sum(count))
    for i in range(num_classes):
        weight_per_class[i] = N/float(count[i])
    weight = [0] * len(ct_instances)
    for idx, val in enumerate(ct_instances):
        weight[idx] = weight_per_class[int(val['label'])]
    return weight

def get_dataset_and_dataloader_ECCV_Challenge2022(
        csvfile_path: str,
        stage: str,
        cls_problem: str = 'covid_cls',
        do_augment: bool= True,
        return_lung_masks: bool = False,
        return_lesions_masks: bool = False,
        return_labels: bool = True,
        batch_size: int = 2,
        num_workers: int = 2,
        num_classes: int= None,
        do_weigthed_sampler=False,
        shuffle: bool = True,
        drop_last: bool = True,
        ratio_unlabeled: float = None,
        semi_supervised: bool = False,
        second_dataset: str = None,
        read_util: int = None):
    """
    :param csvfile_path: path to the csv file with the files (must have at least three columns:
    'image_ct'-> the path to the CT image,
    'image_lung_mask'-> the path to the CT lung mask,
    'label'-> the image Ground truth label,)
    :param stage: the stage (val, train, test, test_independent)
    :param return_lung_masks:
    :param return_lesions_masks:
    :param return_labels: either to return labels or not it this is for BYOL
    :param batch_size: batch_size to use
    :param num_workers: num_workers to use during loading the data
    :param shuffle: either shufle the data or not
    :param drop_last: either drop last batch or not
    :param read_util: read_util...utility for debugginhg with small amount of data.
    :return: ds-> the dataset and loader the dataloader
    """
    ##todo: change print to data loader and add the error checks add csv file structure
    print('Loading training data for {} stage...\n'.format(stage))

    ##idx,hospital,patient_id,image_ct,image_lung_mask,image_lesion_mask,label,pathology

    #####Note to myself notice that ICCV2021 DB.CSV AND ECCV2022 DB.CSV ARE NOT ORGANISED IN THE SAME WAY
    if 'ICCV' in csvfile_path:
        df_sup = pd.read_csv(csvfile_path,
                         sep=",", header=None,
                         names=['image_ct', 'image_lung_mask', 'label'], skiprows=1)
        if stage == 'train' and semi_supervised:
            assert second_dataset is not None, "Not posible to do Semi_Supervised lerning if not Estra unnanotated is provided "
            ##this if is not longer necesary due to the assert but just
            if second_dataset is not None:  ####use the -1 as unsupervised data
                num_classes += 1
                data_extra_df = pd.read_csv(os.path.join(second_dataset,
                                              'training_data.csv'),
                                 sep=";", header=None,
                                 names=['PatientID', 'Subset', 'Age', 'Sex', 'Covid+', 'Severity', 'Covid+ & severe',
                                        'Covid- & severe', 'Image_size_x', 'Image_size_y', 'Image_size_z', 'Spacing_x',
                                        'Spacing_y', 'Spacing_z'], skiprows=1)

                files_ct = os.path.join(second_dataset, 'data/mha/')
                data_extra_df['image_ct'] = files_ct + data_extra_df['PatientID'].map(str) + '.mha'
                files_ct = os.path.join(second_dataset, 'data/lung_masks/')
                data_extra_df['image_lung_mask'] = files_ct + data_extra_df['PatientID'].map(str) + '.nii.gz'
                data_extra_df.rename(columns={'Covid+': 'label'}, inplace=True)
                data_extra_df['label']=-1
                # data_extra_df['category'] = -1
                df_unsup_extra=data_extra_df.loc[:,['image_ct', 'image_lung_mask', 'label']].copy()
                if 0.0 < ratio_unlabeled < 1.0:
                    temp_frac_unsup=df_unsup_extra.sample(frac=ratio_unlabeled)
                    df = pd.concat([df_sup, temp_frac_unsup], ignore_index=True)
                elif ratio_unlabeled > 1.0:
                    temp_frac_unsup = df_unsup_extra.sample(n=int(ratio_unlabeled))
                    df = pd.concat([df_sup, temp_frac_unsup], ignore_index=True)
                else:
                    df = pd.concat([df_sup, df_unsup_extra], ignore_index=True)
        else:
            df=df_sup.copy()
    elif 'ECCV' in csvfile_path:
        data_df = pd.read_csv(csvfile_path,
                         sep=",", header=None,
                         names=['ct_scan_id', 'image_ct', 'image_lung_mask', 'covid', 'category'], skiprows=1)
        if cls_problem == 'covid_cls':
            data_df.rename(columns={'covid': 'label'}, inplace=True)
            df_sup=data_df.loc[:,['image_ct', 'image_lung_mask', 'label']].copy()
            if stage == 'train' and semi_supervised:
                assert second_dataset is not None, "Not posible to do Semi_Supervised lerning if not Estra unnanotated is provided "

                ##this if is not longer necesary due to the assert but just
                if second_dataset is not None:  ####use the -1 as unsupervised data
                    num_classes += 1
                    data_extra_df = pd.read_csv(os.path.join(second_dataset,
                                                             'training_data.csv'),
                                                sep=";", header=None,
                                                names=['PatientID', 'Subset', 'Age', 'Sex', 'Covid+', 'Severity',
                                                       'Covid+ & severe',
                                                       'Covid- & severe', 'Image_size_x', 'Image_size_y',
                                                       'Image_size_z', 'Spacing_x',
                                                       'Spacing_y', 'Spacing_z'], skiprows=1)

                    files_ct = os.path.join(second_dataset, 'data/mha/')
                    data_extra_df['image_ct'] = files_ct + data_extra_df['PatientID'].map(str) + '.mha'
                    files_ct = os.path.join(second_dataset, 'data/lung_masks/')
                    data_extra_df['image_lung_mask'] = files_ct + data_extra_df['PatientID'].map(str) + '.nii.gz'
                    data_extra_df.rename(columns={'Covid+': 'label'}, inplace=True)
                    data_extra_df['label'] = -1###Asumme Unlabeled image
                    df_unsup_extra = data_extra_df.loc[:, ['image_ct', 'image_lung_mask', 'label']].copy()
                    if 0.0 < ratio_unlabeled < 1.0:
                        temp_frac_unsup = df_unsup_extra.sample(frac=ratio_unlabeled)
                        df = pd.concat([df_sup, temp_frac_unsup], ignore_index=True)
                    elif ratio_unlabeled > 1.0:
                        temp_frac_unsup = df_unsup_extra.sample(n=int(ratio_unlabeled))
                        df = pd.concat([df_sup, temp_frac_unsup], ignore_index=True)
                    else:
                        df = pd.concat([df_sup, df_unsup_extra], ignore_index=True)
            else:
                df = df_sup.copy()
        else:
            data_df.rename(columns={'category': 'label'}, inplace=True)
            temp_df = data_df.where(data_df['label'] > 0).copy().loc[:, ['image_ct', 'image_lung_mask', 'label']].copy()
            df_sup = temp_df.dropna(how='all').reset_index(drop=True)
            df_sup['label'] -= 1  ####this is to put categories from 0 to 3
            if stage == 'train' and semi_supervised:
                num_classes+=1
                temp_df_unsup = data_df.where(data_df['label'] < 0).copy().loc[:, ['image_ct', 'image_lung_mask', 'label']].copy()##find less than 0
                df_unsup = temp_df_unsup.dropna(how='all').reset_index(drop=True)
                if second_dataset is not None:  ####use the -1 as unsupervised data
                    data_extra_df = pd.read_csv(os.path.join(second_dataset,
                                                  'training_data.csv'),
                                     sep=";", header=None,
                                     names=['PatientID', 'Subset', 'Age', 'Sex', 'Covid+', 'Severity', 'Covid+ & severe',
                                            'Covid- & severe', 'Image_size_x', 'Image_size_y', 'Image_size_z', 'Spacing_x',
                                            'Spacing_y', 'Spacing_z'], skiprows=1)

                    files_ct = os.path.join(second_dataset, 'data/mha/')
                    data_extra_df['image_ct'] = files_ct + data_extra_df['PatientID'].map(str) + '.mha'
                    files_ct = os.path.join(second_dataset, 'data/lung_masks/')
                    data_extra_df['image_lung_mask'] = files_ct + data_extra_df['PatientID'].map(str) + '.nii.gz'
                    data_extra_df.rename(columns={'Severity': 'label'}, inplace=True)
                    data_extra_df['label']=-1
                    df_unsup_extra=data_extra_df.loc[:,['image_ct', 'image_lung_mask', 'label']].copy()
                    df_unsup_all = pd.concat([df_unsup, df_unsup_extra], ignore_index=True)
                    if 0.0 < ratio_unlabeled < 1.0:
                        temp_frac_unsup=df_unsup_all.sample(frac=ratio_unlabeled)
                        df=pd.concat([df_sup, temp_frac_unsup], ignore_index=True)
                    elif ratio_unlabeled > 1.0:
                        temp_frac_unsup = df_unsup_all.sample(n=int(ratio_unlabeled))
                        df = pd.concat([df_sup, temp_frac_unsup], ignore_index=True)
                    else:
                        df = pd.concat([df_sup, df_unsup_all], ignore_index=True)
                else:
                    if 0.0 < ratio_unlabeled < 1.0:
                        temp_frac_unsup = df_unsup.sample(frac=ratio_unlabeled)
                        df = pd.concat([df_sup, temp_frac_unsup], ignore_index=True)
                    elif ratio_unlabeled > 1.0:
                        temp_frac_unsup = df_unsup.sample(n=int(ratio_unlabeled))
                        df = pd.concat([df_sup, temp_frac_unsup], ignore_index=True)
                    else:
                        df=pd.concat([df_sup, df_unsup], ignore_index=True)
            else:
                df=df_sup.copy()

    df.astype({'label': int})
    files = df.loc[:,['image_ct', 'image_lung_mask', 'label']].copy().to_dict('records')
    if stage=='train':
        if shuffle:
            np.random.shuffle(files)

        labels = [label['label'] for label in files]

        if read_util is not None:
            count_classes = len(np.unique(labels[: read_util]))
        else:
            count_classes = len(np.unique(labels))

        if do_weigthed_sampler:
            assert num_classes == count_classes, "Not posible to do weighted sampling because we found {} classes and number of clases is {} ".format(count_classes,
                                                                                                                                                      num_classes)
        # print('Loading validation data...\n')
        # with open(os.path.join('/home/abel/Project/Data/db5_0/', 'data_{}_files_v_db5_13_10.csv'.format('val')), 'r',
        #           newline='') as csvfile:
        #     reader = csv.DictReader(csvfile)
        #     val_files = []
        #
        #     for row in reader:
        #         row['label'] = np.array(row['label'], dtype=np.uint8)
        #         val_files.append(row)
        #         val_images.append(row['image_ct'])
        #         val_labels.append(row['label'])
        #         # if reader.line_num == 5:
        #         #     break
        # print('Done!!!')

        # # # # Define transforms
        # # train_labels = np.array(train_labels, dtype=np.int64)
        # # val_labels = np.array(val_labels,dtype=np.int64)
        # # # train_transforms = Compose([ScaleIntensity(), AddChannel(), Resize((128, 128, 128)), RandRotate90(), EnsureType()])
        # # # val_transforms = Compose([ScaleIntensity(), AddChannel(), Resize((128, 128, 128)), EnsureType()])

        # keys = ('image_ct', 'image_lung_mask', 'image_lesion_mask', 'label')

    keys = ('image_ct', 'image_lung_mask', 'label')

    if stage !="train":
        do_augment = False

    data_transform = get_data_transforms_ECCV_Challenge2022(stage=stage,
                                         keys=keys,
                                         do_augment=do_augment,
                                         return_lung_masks=return_lung_masks,
                                         return_lesions_masks=return_lesions_masks,
                                         return_labels=return_labels)

    data_transform.set_random_state(seed=42)

    # #Sanity checks!!!
    # check_ds = Dataset(data=files, transform=data_transform)
    # check_loader = DataLoader(check_ds, batch_size=batch_size, num_workers=num_workers,
    #                           pin_memory=torch.cuda.is_available())
    # check_data = monai.utils.misc.first(check_loader)
    # im, label = check_data[keys[0]], check_data[keys[2]]
    # print(type(im), f"image shape: {im.shape}", label)
    #
    # # #
    # # ## create a figure of a 3D volume
    # # # volume = np.random.rand(10, 10, 10)
    # # # fig = plt.figure()
    # # # matshow3d(volume, fig=fig, title="3D Volume")
    # # # plt.show()
    # # # # create a figure of a list of channel-first 3D volumes
    # # # volumes = [np.random.rand(1, 10, 10, 10), np.random.rand(1, 10, 10, 10)]
    # # # fig = plt.figure()
    # # # matshow3d(volumes, fig=fig, title="List of Volumes")
    # # # plt.show()
    # from monai.visualize import matshow3d
    # for idx, vol in enumerate(im):
    #     plt.set_cmap("gray")
    #     fig = plt.figure()
    #     matshow3d(vol, fig=fig, title="Volume_at_index{}".format(idx), frames_per_row=10,vmin=0,vmax=1, margin=3, frame_dim=-1)
    #     #plt.show()
    #     os.makedirs('./temp')
    #     plt.savefig('./temp/'+'plot_Example_3D_vol_{}.jpeg'.format(idx))
    #     plt.close(fig=fig)

    if read_util is not None:
        ds = CacheDataset(data=files[: read_util],
                          transform=data_transform, progress=True)  ###used to debug with smoll amount of data
    else:
        ds = CacheDataset(data=files, transform=data_transform, progress=True)


    if stage=='train':
        if do_weigthed_sampler and num_classes>1: ###mutually exclusive
            weights = make_weights_for_balanced_classes(ds.data, num_classes)
            weights = torch.DoubleTensor(weights)
            sampler = torch.utils.data.sampler.WeightedRandomSampler(weights, len(weights))

            loader = DataLoader(ds, sampler=sampler, batch_size=batch_size, num_workers=num_workers, shuffle=False,
                                pin_memory=torch.cuda.is_available(),
                                drop_last=drop_last)
        elif do_weigthed_sampler and num_classes == 1:  ###not possible to do weigthed_sampler
            print('Dataset sample set to shufle as only one class was found in the data.')
            loader = DataLoader(ds, batch_size=batch_size, shuffle=True, num_workers=num_workers,
                                pin_memory=torch.cuda.is_available(),
                                drop_last=drop_last)
        else:
            loader = DataLoader(ds, batch_size=batch_size, shuffle=True, num_workers=num_workers,
                                pin_memory=torch.cuda.is_available(),
                                drop_last=drop_last)
    else:
        loader = DataLoader(ds, batch_size=batch_size, shuffle=False, num_workers=num_workers,
                            pin_memory=torch.cuda.is_available(),
                            drop_last=False)

    return ds, loader

def get_dataset_and_dataloader_ECCV_Challenge2022_Eval(
        csvfile_path: str,
        stage: str,
        cls_problem: str = 'covid_cls',
        do_augment: bool= True,
        return_lung_masks: bool = False,
        return_lesions_masks: bool = False,
        return_labels: bool = True,
        batch_size: int = 2,
        num_workers: int = 2,
        num_classes: int= None,
        do_weigthed_sampler=False,
        shuffle: bool = True,
        drop_last: bool = True,
        ratio_unlabeled: float = None,
        semi_supervised: bool = False,
        second_dataset: str = None,
        read_util: int = None):
    """
    :param csvfile_path: path to the csv file with the files (must have at least three columns:
    'image_ct'-> the path to the CT image,
    'image_lung_mask'-> the path to the CT lung mask,
    'label'-> the image Ground truth label,)
    :param stage: the stage (val, train, test, test_independent)
    :param return_lung_masks:
    :param return_lesions_masks:
    :param return_labels: either to return labels or not it this is for BYOL
    :param batch_size: batch_size to use
    :param num_workers: num_workers to use during loading the data
    :param shuffle: either shufle the data or not
    :param drop_last: either drop last batch or not
    :param read_util: read_util...utility for debugginhg with small amount of data.
    :return: ds-> the dataset and loader the dataloader
    """
    ##todo: change print to data loader and add the error checks add csv file structure
    print('Loading training data for {} stage...\n'.format(stage))

    if 'ICCV' in csvfile_path:
        df_sup = pd.read_csv(csvfile_path,
                         sep=",", header=None,
                         names=['image_ct', 'image_lung_mask', 'label'], skiprows=1)
        if stage == 'train' and semi_supervised:
            assert second_dataset is not None, "Not posible to do Semi_Supervised lerning if not Estra unnanotated is provided "
            ##this if is not longer necesary due to the assert but just
            if second_dataset is not None:  ####use the -1 as unsupervised data
                num_classes += 1
                data_extra_df = pd.read_csv(os.path.join(second_dataset,
                                              'training_data.csv'),
                                 sep=";", header=None,
                                 names=['PatientID', 'Subset', 'Age', 'Sex', 'Covid+', 'Severity', 'Covid+ & severe',
                                        'Covid- & severe', 'Image_size_x', 'Image_size_y', 'Image_size_z', 'Spacing_x',
                                        'Spacing_y', 'Spacing_z'], skiprows=1)

                files_ct = os.path.join(second_dataset, 'data/mha/')
                data_extra_df['image_ct'] = files_ct + data_extra_df['PatientID'].map(str) + '.mha'
                files_ct = os.path.join(second_dataset, 'data/lung_masks/')
                data_extra_df['image_lung_mask'] = files_ct + data_extra_df['PatientID'].map(str) + '.nii.gz'
                data_extra_df.rename(columns={'Covid+': 'label'}, inplace=True)
                data_extra_df['label']=-1
                # data_extra_df['category'] = -1
                df_unsup_extra=data_extra_df.loc[:,['image_ct', 'image_lung_mask', 'label']].copy()
                if 0.0 < ratio_unlabeled < 1.0:
                    temp_frac_unsup=df_unsup_extra.sample(frac=ratio_unlabeled)
                    df = pd.concat([df_sup, temp_frac_unsup], ignore_index=True)
                elif ratio_unlabeled > 1.0:
                    temp_frac_unsup = df_unsup_extra.sample(n=int(ratio_unlabeled))
                    df = pd.concat([df_sup, temp_frac_unsup], ignore_index=True)
                else:
                    df = pd.concat([df_sup, df_unsup_extra], ignore_index=True)
        else:
            df=df_sup.copy()
    elif 'ECCV' in csvfile_path:
        data_df = pd.read_csv(csvfile_path,
                         sep=",", header=None,
                         names=['ct_scan_id', 'image_ct', 'image_lung_mask', 'covid', 'category'], skiprows=1)
        if cls_problem == 'covid_cls':
            data_df.rename(columns={'covid': 'label'}, inplace=True)
            df_sup=data_df.loc[:,['image_ct', 'image_lung_mask', 'label']].copy()
            if stage == 'train' and semi_supervised:
                assert second_dataset is not None, "Not posible to do Semi_Supervised lerning if not Estra unnanotated is provided "

                ##this if is not longer necesary due to the assert but just
                if second_dataset is not None:  ####use the -1 as unsupervised data
                    num_classes += 1
                    data_extra_df = pd.read_csv(os.path.join(second_dataset,
                                                             'training_data.csv'),
                                                sep=";", header=None,
                                                names=['PatientID', 'Subset', 'Age', 'Sex', 'Covid+', 'Severity',
                                                       'Covid+ & severe',
                                                       'Covid- & severe', 'Image_size_x', 'Image_size_y',
                                                       'Image_size_z', 'Spacing_x',
                                                       'Spacing_y', 'Spacing_z'], skiprows=1)

                    files_ct = os.path.join(second_dataset, 'data/mha/')
                    data_extra_df['image_ct'] = files_ct + data_extra_df['PatientID'].map(str) + '.mha'
                    files_ct = os.path.join(second_dataset, 'data/lung_masks/')
                    data_extra_df['image_lung_mask'] = files_ct + data_extra_df['PatientID'].map(str) + '.nii.gz'
                    data_extra_df.rename(columns={'Covid+': 'label'}, inplace=True)
                    data_extra_df['label'] = -1###Asumme Unlabeled image
                    df_unsup_extra = data_extra_df.loc[:, ['image_ct', 'image_lung_mask', 'label']].copy()
                    if 0.0 < ratio_unlabeled < 1.0:
                        temp_frac_unsup = df_unsup_extra.sample(frac=ratio_unlabeled)
                        df = pd.concat([df_sup, temp_frac_unsup], ignore_index=True)
                    elif ratio_unlabeled > 1.0:
                        temp_frac_unsup = df_unsup_extra.sample(n=int(ratio_unlabeled))
                        df = pd.concat([df_sup, temp_frac_unsup], ignore_index=True)
                    else:
                        df = pd.concat([df_sup, df_unsup_extra], ignore_index=True)
            else:
                df = df_sup.copy()
        else:
            data_df.rename(columns={'category': 'label'}, inplace=True)
            if stage == 'test':
                df_sup=data_df.copy()
            elif stage == 'val':
                temp_df = data_df.where(data_df['label'] > 0).copy().loc[:, ['image_ct', 'image_lung_mask', 'label']].copy()
                df_sup = temp_df.dropna(how='all').reset_index(drop=True)
                df_sup['label'] -= 1  ####this is to put categories from 0 to 3
            df=df_sup.copy()

    df.astype({'label': int})
    files = df.loc[:,['image_ct', 'image_lung_mask', 'label']].copy().to_dict('records')
    if stage=='train':
        if shuffle:
            np.random.shuffle(files)
            # values of labels in list of dictionaries

        labels = [label['label'] for label in files]

        if read_util is not None:
            count_classes = len(np.unique(labels[: read_util]))
        else:
            count_classes = len(np.unique(labels))

        if do_weigthed_sampler:
            assert num_classes == count_classes, "Not posible to do weighted sampling because we found {} classes and number of clases is {} ".format(count_classes,
                                                                                                                                                      num_classes)

    keys = ('image_ct', 'image_lung_mask', 'label')

    if stage !="train":
        do_augment = False

    data_transform = get_data_transforms_ECCV_Challenge2022(stage=stage,
                                         keys=keys,
                                         do_augment=do_augment,
                                         return_lung_masks=return_lung_masks,
                                         return_lesions_masks=return_lesions_masks,
                                         return_labels=return_labels)

    data_transform.set_random_state(seed=42)

    # #Sanity checks!!!
    # check_ds = Dataset(data=files, transform=data_transform)
    # check_loader = DataLoader(check_ds, batch_size=batch_size, num_workers=num_workers,
    #                           pin_memory=torch.cuda.is_available())
    # check_data = monai.utils.misc.first(check_loader)
    # im, label = check_data[keys[0]], check_data[keys[2]]
    # print(type(im), f"image shape: {im.shape}", label)
    #
    # # #
    # # ## create a figure of a 3D volume
    # # # volume = np.random.rand(10, 10, 10)
    # # # fig = plt.figure()
    # # # matshow3d(volume, fig=fig, title="3D Volume")
    # # # plt.show()
    # # # # create a figure of a list of channel-first 3D volumes
    # # # volumes = [np.random.rand(1, 10, 10, 10), np.random.rand(1, 10, 10, 10)]
    # # # fig = plt.figure()
    # # # matshow3d(volumes, fig=fig, title="List of Volumes")
    # # # plt.show()
    # from monai.visualize import matshow3d
    # for idx, vol in enumerate(im):
    #     plt.set_cmap("gray")
    #     fig = plt.figure()
    #     matshow3d(vol, fig=fig, title="Volume_at_index{}".format(idx), frames_per_row=10,vmin=0,vmax=1, margin=3, frame_dim=-1)
    #     #plt.show()
    #     os.makedirs('./temp')
    #     plt.savefig('./temp/'+'plot_Example_3D_vol_{}.jpeg'.format(idx))
    #     plt.close(fig=fig)

    if read_util is not None:
        ds = CacheDataset(data=files[: read_util],
                          transform=data_transform, progress=True)  ###used to debug with smoll amount of data
    else:
        ds = CacheDataset(data=files, transform=data_transform, progress=True)


    if stage=='train':
        if do_weigthed_sampler and num_classes>1: ###mutually exclusive
            weights = make_weights_for_balanced_classes(ds.data, num_classes)
            weights = torch.DoubleTensor(weights)
            sampler = torch.utils.data.sampler.WeightedRandomSampler(weights, len(weights))

            loader = DataLoader(ds, sampler=sampler, batch_size=batch_size, num_workers=num_workers, shuffle=False,
                                pin_memory=torch.cuda.is_available(),
                                drop_last=drop_last)
        elif do_weigthed_sampler and num_classes == 1:  ###not possible to do weigthed_sampler
            print('Dataset sample set to shufle as only one class was found in the data.')
            loader = DataLoader(ds, batch_size=batch_size, shuffle=True, num_workers=num_workers,
                                pin_memory=torch.cuda.is_available(),
                                drop_last=drop_last)
        else:
            loader = DataLoader(ds, batch_size=batch_size, shuffle=True, num_workers=num_workers,
                                pin_memory=torch.cuda.is_available(),
                                drop_last=drop_last)
    else:
        loader = DataLoader(ds, batch_size=batch_size, shuffle=False, num_workers=num_workers,
                            pin_memory=torch.cuda.is_available(),
                            drop_last=False)


    return ds, loader, files

def get_data_transforms(stage: str = "train",
                        do_augment: bool = True,
                        keys: tuple = ('image_ct', 'image_lung_mask', 'image_lesion_mask','label'),
                        min_hu_intensity: int = -1000,
                        max_hu_intensity: int = 500,
                        in_plane_x: int = 224,
                        in_plane_y: int = 224,
                        number_slices: int = 112,
                        return_lung_masks: bool = False,
                        return_lesions_masks: bool = False,
                        return_labels: bool = True):
    """
    :param stage:
    :param do_augment:
    :param keys:
    :param min_hu_intensity:
    :param max_hu_intensity:
    :param in_plane_x:
    :param in_plane_y:
    :param number_slices:
    :param return_lung_masks:
    :param return_lesions_masks:
    :param return_labels:
    :return:
    """
    assert (keys == ('image_ct', 'image_lung_mask', 'image_lesion_mask','label')
            or keys == ('image_ct', 'image_lung_mask','label')), "These transforms require another keys input"
    xforms = [
        LoadImaged(keys=keys[:-1], reader='NibabelReader', allow_missing_keys=True),
        AddChanneld(keys=keys[:-1], allow_missing_keys=True),
        Orientationd(keys=keys[:-1], axcodes="RAS", allow_missing_keys=True),
        MaskIntensityd(keys=keys[0], mask_key=keys[1], allow_missing_keys=True),
        ScaleIntensityRanged(keys=keys[0], a_min=min_hu_intensity,
                             a_max=max_hu_intensity, b_min=0.0, b_max=1.0, clip=True, allow_missing_keys=True),
        CropForegroundd(keys=keys[:-1], source_key=keys[1], margin=(5, 5, 0), select_fn=lambda x: x > 0, allow_missing_keys=True),
        Resized(keys=keys[:-1], spatial_size=in_plane_x,
                size_mode='longest', mode='trilinear', align_corners=True, allow_missing_keys=True),
        ResizeWithPadOrCropd(keys=keys[:-1], spatial_size=(in_plane_x, in_plane_y, number_slices),
                             mode='constant', allow_missing_keys=True),
        Rotate90d(keys=keys[:-1], k=3, allow_missing_keys=True),
        Flipd(keys=keys[:-1], spatial_axis=0, allow_missing_keys=True),
    ]
    if stage == "train" and do_augment:
        xforms.extend(
            [RandRotate90d(
                keys=keys[:-1], prob=0.5, allow_missing_keys=True
            ),
                RandFlipd(keys=keys[:-1],
                          spatial_axis=(0,1),
                          prob=0.5, allow_missing_keys=True
                          ),
                RandShiftIntensityd(
                    keys=keys[0],
                    offsets=0.10,
                    prob=0.5, allow_missing_keys=True),

                RandHistogramShiftd(keys=keys[0],
                    num_control_points=10,
                    prob=0.5, allow_missing_keys=True),

                RandAffined(keys=keys[:-1],
                            prob=0.5,
                            rotate_range=(-0.05, 0.05),
                            scale_range=(-0.1, 0.1),
                            mode=("bilinear"),
                            as_tensor_output=False, allow_missing_keys=True
                            ),
                RandGaussianSmoothd(keys=keys[0],
                                    sigma_x=(0.5, 1.15),
                                    sigma_y=(0.5, 1.15),
                                    sigma_z=(0.5, 1.15),
                                    prob=0.3, allow_missing_keys=True
                                    ),
                RandAdjustContrastd(keys=keys[0], prob=0.5,
                                    gamma=(0.5, 4.5), allow_missing_keys=True
                                    ),
                RandZoomd(keys=keys[:-1],
                          min_zoom=0.6,
                          max_zoom=0.9,
                          prob=0.5, allow_missing_keys=True, padding_mode='constant'
                          ),
                RandGaussianNoised(keys=keys[0],
                                   prob=0.3,
                                   std=0.01, allow_missing_keys=True),
            ]
        )
 ###todo ??maybe only lesion ans ct? so far not needed.
    if keys == ('image_ct', 'image_lung_mask', 'image_lesion_mask', 'label'):
        if return_lung_masks and return_lesions_masks and return_labels:## in case the three images are required
            MaskIntensityd(keys=keys[:-1], mask_key=keys[1],allow_missing_keys=True),  ####remove noise introduced during the augmentations!
            dtype = (np.float32, np.float32, np.float32, np.int8)
            xforms.extend([CastToTyped(keys=keys, dtype=dtype), ToTensord(keys=keys)])
        elif return_lung_masks and not return_lesions_masks and return_labels:## in case the lungmask and lung lesions are required
                MaskIntensityd(keys=keys[:-2], mask_key=keys[1], allow_missing_keys=True),  ####remove noise introduced during the augmentations!
                DeleteItemsd(keys=['image_lesion_mask']),
                dtype = (np.float32, np.float32, np.int8)
                xforms.extend(
                    [CastToTyped(keys=['image_ct', 'image_lung_mask', 'label'], dtype=dtype),
                     ToTensord(keys=['image_ct', 'image_lung_mask', 'label'])])
        elif not return_lung_masks and not return_lesions_masks and return_labels:###normally use, return CT and Labels
            MaskIntensityd(keys=keys[0], mask_key=keys[1],allow_missing_keys=True),  ####remove noise introduced during the augmentations!
            DeleteItemsd(keys=['image_lung_mask', 'image_lesion_mask']),
            dtype = (np.float32, np.int8)
            xforms.extend(
                [CastToTyped(keys=['image_ct','label'], dtype=dtype),
                 ToTensord(keys=['image_ct','label'])])
        else:##return only CT image
            MaskIntensityd(keys=keys[0], mask_key=keys[1],allow_missing_keys=True),  ####remove noise introduced during the augmentations!
            DeleteItemsd(keys=['image_lung_mask', 'image_lesion_mask', 'label']),
            dtype = (np.float32)
            xforms.extend([CastToTyped(keys=['image_ct'], dtype=dtype), ToTensord(keys=['image_ct'])])
    elif keys == ('image_ct', 'image_lung_mask', 'label'):
        if return_lung_masks and return_labels:
            MaskIntensityd(keys=keys[:-1], mask_key=keys[1],allow_missing_keys=True),  ####remove noise introduced during the augmentations!
            dtype = (np.float32, np.float32, np.float32, np.int8)
            xforms.extend([CastToTyped(keys=['image_ct', 'image_lung_mask', 'label'], dtype=dtype),
                           ToTensord(keys=['image_ct', 'image_lung_mask', 'label'])])
        elif not return_lung_masks and return_labels:
            MaskIntensityd(keys=keys[0], mask_key=keys[1],allow_missing_keys=True),  ####remove noise introduced during the augmentations!
            DeleteItemsd(keys=['image_lung_mask']),
            dtype = (np.float32, np.int8)
            xforms.extend(
                [CastToTyped(keys=['image_ct','label'], dtype=dtype),
                 ToTensord(keys=['image_ct','label'])])
        else:##return only CT image
            MaskIntensityd(keys=keys[0], mask_key=keys[1],allow_missing_keys=True),  ####remove noise introduced during the augmentations!
            DeleteItemsd(keys=['image_lung_mask', 'label']),
            dtype = (np.float32)
            xforms.extend([CastToTyped(keys=['image_ct'], dtype=dtype), ToTensord(keys=['image_ct'])])
    else:
        raise NotImplementedError(f"NOt sure if we must implement this extra option method.")
    return monai.transforms.Compose(xforms)

####*-*-*-*-*-*-*-*-STOIC-DATA
def get_dataset_and_dataloader_STOIC_DATA(
        files_dict: dict,
        stage: str,
        do_augment: bool,
        return_lung_masks: bool = False,
        return_lesions_masks: bool = False,
        return_labels: bool = True,
        batch_size: int = 2,
        num_workers: int = 2,
        num_classes: int= 2,
        do_weigthed_sampler=False,
        shuffle: bool = True,
        drop_last: bool = True,
        ratio_unlabeled: float = None,
        semi_supervised: bool = False,
        read_util: int = None):
    """
    :param csvfile_path: path to the csv file with the files (must have at least three columns:
    'image_ct'-> the path to the CT image,
    'image_lung_mask'-> the path to the CT lung mask,
    'label'-> the image Ground truth label,)
    :param stage: the stage (val, train, test, test_independent)
    :param return_lung_masks:
    :param return_lesions_masks:
    :param return_labels: either to return labels or not it this is for BYOL
    :param batch_size: batch_size to use
    :param num_workers: num_workers to use during loading the data
    :param shuffle: either shufle the data or not
    :param drop_last: either drop last batch or not
    :param read_util: read_util...utility for debugginhg with small amount of data.
    :return: ds-> the dataset and loader the dataloader
    """
    ##todo: change print to data loader and add the error checks add csv file structure
    print('Loading training data for {} stage...\n'.format(stage))

    ##idx,hospital,patient_id,image_ct,image_lung_mask,image_lesion_mask,label,pathology

    # if os.path.exists(csvfile_path): ### TODO not the best to do
    #     files = list()
    #     with open(csvfile_path, 'r',
    #               newline='') as csvfile:
    #         reader = csv.DictReader(csvfile)
    #         ##todo eliminate this loop
    #         if stage =='train' and ratio_unlabeled is not None:
    #             labels=[]
    #         # for row in reader:
    #         for idx, row in enumerate(reader):
    #             ###np.array is required for labels
    #             row['label'] = np.array(row['label'], dtype=np.int8)
    #             if stage =='train' and ratio_unlabeled is not None:
    #                 labels.append(row['label'])
    #             files.append(row)

    files=files_dict

    if stage == 'train' and ratio_unlabeled is not None:
        labels=[]
        for item in files:
            labels.append(item['label'])

    if stage =='train' and ratio_unlabeled is not None:
        labeled_idx, unlabeled_idx = train_test_split(np.arange(len(labels)), test_size=ratio_unlabeled, random_state=42, shuffle=True,
                                                stratify=labels)
        if semi_supervised:
            for idx in unlabeled_idx:
                files[idx]['label'] = np.array(-1, dtype=np.int8)
            num_classes = num_classes + 1
        else:
            temp = [files[i] for i in labeled_idx]
            files=temp

    # print('Loading validation data...\n')
    # with open(os.path.join('/home/abel/Project/Data/db5_0/', 'data_{}_files_v_db5_13_10.csv'.format('val')), 'r',
    #           newline='') as csvfile:
    #     reader = csv.DictReader(csvfile)
    #     val_files = []
    #
    #     for row in reader:
    #         row['label'] = np.array(row['label'], dtype=np.uint8)
    #         val_files.append(row)
    #         val_images.append(row['image_ct'])
    #         val_labels.append(row['label'])
    #         # if reader.line_num == 5:
    #         #     break
    # print('Done!!!')

    # # # # Define transforms
    # # train_labels = np.array(train_labels, dtype=np.int64)
    # # val_labels = np.array(val_labels,dtype=np.int64)
    # # # train_transforms = Compose([ScaleIntensity(), AddChannel(), Resize((128, 128, 128)), RandRotate90(), EnsureType()])
    # # # val_transforms = Compose([ScaleIntensity(), AddChannel(), Resize((128, 128, 128)), EnsureType()])

    # keys = ('image_ct', 'image_lung_mask', 'image_lesion_mask', 'label')

    keys = ('image_ct', 'image_lung_mask', 'label')

    if stage !="train":
        do_augment = False

    data_transform = get_data_transforms_STOIC_DATA(stage=stage,
                                         keys=keys,
                                         do_augment=do_augment,
                                         return_lung_masks=return_lung_masks,
                                         return_lesions_masks=return_lesions_masks,
                                         return_labels=return_labels)

    data_transform.set_random_state(seed=42)

    # # #Sanity checks!!!
    # check_ds = Dataset(data=files, transform=data_transform)
    # check_loader = DataLoader(check_ds, batch_size=batch_size, num_workers=num_workers,
    #                           pin_memory=torch.cuda.is_available())
    # check_data = monai.utils.misc.first(check_loader)
    # ###check_data['image_ct_meta_dict']--->>>>metadata
    # # str(check_data['image_ct_meta_dict']['PatientSex'][0])
    # # int(check_data['image_ct_meta_dict']['PatientAge'][0])
    # im, label = check_data[keys[0]], check_data[keys[2]]###Normally is 2
    # print(type(im), f"image shape: {im.shape}", label)
    #
    # # #
    # # ## create a figure of a 3D volume
    # # # volume = np.random.rand(10, 10, 10)
    # # # fig = plt.figure()
    # # # matshow3d(volume, fig=fig, title="3D Volume")
    # # # plt.show()
    # # # # create a figure of a list of channel-first 3D volumes
    # # # volumes = [np.random.rand(1, 10, 10, 10), np.random.rand(1, 10, 10, 10)]
    # # # fig = plt.figure()
    # # # matshow3d(volumes, fig=fig, title="List of Volumes")
    # # # plt.show()
    # # from monai.visualize import matshow3d
    # # for idx, vol in enumerate(im):
    # #     plt.set_cmap("gray")
    # #     fig = plt.figure()
    # #     matshow3d(vol, fig=fig, title="Volume_at_index{}".format(idx), frames_per_row=10,vmin=0,vmax=1, margin=3, frame_dim=-1)
    # #     #plt.show()
    # #     # os.makedirs('./temp')
    # #     plt.savefig('./temp/'+'plot_Example_3D_vol_{}.jpeg'.format(idx))
    # #     plt.close(fig=fig)

    if read_util is not None:
        ds = CacheDataset(data=files[: read_util],
                          transform=data_transform)  ###used to ddebug with smoll amount of data
    else:
        ds = CacheDataset(data=files, transform=data_transform)

    if do_weigthed_sampler: ###mutually exclusive
        shuffle=False

    if stage=='train':
        if do_weigthed_sampler:
            weights = make_weights_for_balanced_classes(ds.data, num_classes)
            weights = torch.DoubleTensor(weights)
            # sampler = DistributedWeightedRandomSampler(weights, len(weights), shuffle=shuffle)
            sampler = torch.utils.data.sampler.WeightedRandomSampler(weights, len(weights))

            loader = DataLoader(ds, sampler=sampler, batch_size=batch_size, num_workers=num_workers,
                                pin_memory=torch.cuda.is_available(),
                                drop_last=drop_last)
        elif shuffle:
            loader = DataLoader(ds, batch_size=batch_size, shuffle=shuffle, num_workers=num_workers,
                                pin_memory=torch.cuda.is_available(),
                                drop_last=drop_last)
    else:
        loader = DataLoader(ds, batch_size=batch_size, shuffle=False, num_workers=num_workers,
                            pin_memory=torch.cuda.is_available(),
                            drop_last=drop_last)

    # loader = DataLoader(ds, batch_size=batch_size, shuffle=shuffle, num_workers=num_workers,
    #                         pin_memory=torch.cuda.is_available(),
    #                         drop_last=drop_last)

    return ds, loader

def get_data_transforms_ECCV_Challenge2022(stage: str = "train",
                        do_augment: bool = True,
                        keys: tuple = ('image_ct', 'image_lung_mask', 'image_lesion_mask','label'),
                        min_hu_intensity: int = -1000,
                        max_hu_intensity: int = 500,
                        in_plane_x: int = 224,
                        in_plane_y: int = 224,
                        number_slices: int = 112,
                        return_lung_masks: bool = False,
                        return_lesions_masks: bool = False,
                        return_labels: bool = True):
    """
    :param stage:
    :param do_augment:
    :param keys:
    :param min_hu_intensity:
    :param max_hu_intensity:
    :param in_plane_x:
    :param in_plane_y:
    :param number_slices:
    :param return_lung_masks:
    :param return_lesions_masks:
    :param return_labels:
    :return:
    """
    assert (keys == ('image_ct', 'image_lung_mask', 'image_lesion_mask','label')
            or keys == ('image_ct', 'image_lung_mask','label')), "These transforms require another keys input"
    xforms = [
        LoadImaged(keys=keys[:-1], reader='itkreader', allow_missing_keys=True, image_only=True),
        AddChanneld(keys=keys[:-1], allow_missing_keys=True),
        Orientationd(keys=keys[:-1], axcodes="RAS", allow_missing_keys=True),
        MaskIntensityd(keys=keys[0], mask_key=keys[1], allow_missing_keys=True),
        ScaleIntensityRanged(keys=keys[0], a_min=min_hu_intensity,
                             a_max=max_hu_intensity, b_min=0.0, b_max=1.0, clip=True, allow_missing_keys=True),
        CropForegroundd(keys=keys[:-1], source_key=keys[1], margin=(5, 5, 0), select_fn=lambda x: x > 0, allow_missing_keys=True),
        Resized(keys=keys[:-1], spatial_size=in_plane_x,
                size_mode='longest', mode='trilinear', align_corners=True, allow_missing_keys=True),
        ResizeWithPadOrCropd(keys=keys[:-1], spatial_size=(in_plane_x, in_plane_y, number_slices),
                             mode='constant', allow_missing_keys=True),
        Rotate90d(keys=keys[:-1], k=3, allow_missing_keys=True),
        Flipd(keys=keys[:-1], spatial_axis=0, allow_missing_keys=True),
    ]
    if stage == "train" and do_augment:
        xforms.extend(
            [RandRotate90d(
                keys=keys[:-1], prob=0.5, allow_missing_keys=True
            ),
                RandFlipd(keys=keys[:-1],
                          spatial_axis=(0,1),
                          prob=0.5, allow_missing_keys=True
                          ),
                RandShiftIntensityd(
                    keys=keys[0],
                    offsets=0.10,
                    prob=0.5, allow_missing_keys=True),

                RandHistogramShiftd(keys=keys[0],
                    num_control_points=10,
                    prob=0.5, allow_missing_keys=True),

                RandAffined(keys=keys[:-1],
                            prob=0.5,
                            rotate_range=(-0.05, 0.05),
                            scale_range=(-0.1, 0.1),
                            mode=("bilinear"),
                            as_tensor_output=False, allow_missing_keys=True
                            ),
                RandGaussianSmoothd(keys=keys[0],
                                    sigma_x=(0.5, 1.15),
                                    sigma_y=(0.5, 1.15),
                                    sigma_z=(0.5, 1.15),
                                    prob=0.3, allow_missing_keys=True
                                    ),
                RandAdjustContrastd(keys=keys[0], prob=0.5,
                                    gamma=(0.5, 4.5), allow_missing_keys=True
                                    ),
                RandZoomd(keys=keys[:-1],
                          min_zoom=0.6,
                          max_zoom=0.9,
                          prob=0.5, allow_missing_keys=True, padding_mode='constant'
                          ),
                RandGaussianNoised(keys=keys[0],
                                   prob=0.3,
                                   std=0.01, allow_missing_keys=True),
            ]
        )
 ###todo ??maybe only lesion ans ct? so far not needed.
    if keys == ('image_ct', 'image_lung_mask', 'image_lesion_mask', 'label'):
        if return_lung_masks and return_lesions_masks and return_labels:## in case the three images are required
            MaskIntensityd(keys=keys[:-1], mask_key=keys[1],allow_missing_keys=True),  ####remove noise introduced during the augmentations!
            xforms.extend([EnsureTyped(keys=keys)])
        elif return_lung_masks and not return_lesions_masks and return_labels:## in case the lungmask and lung lesions are required
                MaskIntensityd(keys=keys[:-2], mask_key=keys[1], allow_missing_keys=True),  ####remove noise introduced during the augmentations!
                DeleteItemsd(keys=['image_lesion_mask']),
                xforms.extend(
                    [EnsureTyped(keys=['image_ct', 'image_lung_mask', 'label'])])
        elif not return_lung_masks and not return_lesions_masks and return_labels:###normally use, return CT and Labels
            MaskIntensityd(keys=keys[0], mask_key=keys[1],allow_missing_keys=True),  ####remove noise introduced during the augmentations!
            DeleteItemsd(keys=['image_lung_mask', 'image_lesion_mask']),
            xforms.extend(
                [EnsureTyped(keys=['image_ct','label'])])
        else:##return only CT image
            MaskIntensityd(keys=keys[0], mask_key=keys[1],allow_missing_keys=True),  ####remove noise introduced during the augmentations!
            DeleteItemsd(keys=['image_lung_mask', 'image_lesion_mask', 'label']),
            xforms.extend([EnsureTyped(keys=['image_ct'])])
    elif keys == ('image_ct', 'image_lung_mask', 'label'):
        if return_lung_masks and return_labels:
            MaskIntensityd(keys=keys[:-1], mask_key=keys[1],allow_missing_keys=True),  ####remove noise introduced during the augmentations!
            xforms.extend([EnsureTyped(keys=['image_ct', 'image_lung_mask', 'label'])])
        elif not return_lung_masks and return_labels:
            MaskIntensityd(keys=keys[0], mask_key=keys[1],allow_missing_keys=True),  ####remove noise introduced during the augmentations!
            DeleteItemsd(keys=['image_lung_mask']),
            xforms.extend(
                [EnsureTyped(keys=['image_ct','label'])])
        else:##return only CT image
            MaskIntensityd(keys=keys[0], mask_key=keys[1],allow_missing_keys=True),  ####remove noise introduced during the augmentations!
            DeleteItemsd(keys=['image_lung_mask', 'label']),
            xforms.extend([EnsureTyped(keys=['image_ct'])])
    else:
        raise NotImplementedError(f"NOt sure if we must implement this extra option method.")
    return monai.transforms.Compose(xforms)
##########################
### Computing metrics
##########################
#FROM DINO
def cosine_scheduler(base_value, final_value, epochs, niter_per_ep, warmup_epochs=0, start_warmup_value=0, warmup_steps=-1):
    warmup_schedule = np.array([])
    warmup_iters = warmup_epochs * niter_per_ep
    if warmup_steps > 0:
        warmup_iters = warmup_steps
    print("Set warmup steps = %d" % warmup_iters)
    if warmup_epochs > 0:
        warmup_schedule = np.linspace(start_warmup_value, base_value, warmup_iters)

    iters = np.arange(epochs * niter_per_ep - warmup_iters)
    schedule = final_value + 0.5 * (base_value - final_value) * (1 + np.cos(np.pi * iters / len(iters)))

    schedule = np.concatenate((warmup_schedule, schedule))
    assert len(schedule) == epochs * niter_per_ep
    return schedule
##########################
### Computing metrics
##########################
def compute_roc_auc(labels, prediction, path_to_save=None, stage=None):
    fpr, tpr, thresholds = roc_curve(labels, prediction, pos_label=1)
    roc_auc = auc(fpr, tpr)
    # print("TPR value: {}".format(tpr))
    # print("FPR value: {}".format(tpr))
    # print(thresholds)
    gmeans = np.sqrt(tpr * (1 - fpr))
    ## locate the index of the largest g-mean
    ix = np.argmax(gmeans)
    # print('Best Threshold=%f, G-Mean=%.3f' % (thresholds[ix], gmeans[ix]))

    if path_to_save and stage is not None:
        plt.figure()
        lw = 1
        plt.plot(fpr, tpr, color='darkorange',
                 lw=lw, label='ROC curve (AUC = %0.2f)' % roc_auc)

        plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.scatter(fpr[ix], tpr[ix], marker='o', color='black', label='Best point')
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('ROC')
        plt.legend(loc="lower right")
        plt.savefig(os.path.join(path_to_save, 'STOC+ROC_{}.pdf'.format(stage)), format="pdf", bbox_inches="tight")
        plt.close('all')
    return roc_auc


def compute_prc(labels, prediction, path_to_save=None, stage=None):
    # calculate precision and recall
    precision, recall, thresholds = precision_recall_curve(labels, prediction)

    ###TODO_refine this

    if path_to_save and stage is not None:
        # create precision recall curve
        fig, ax = plt.subplots()
        ax.plot(recall, precision, color='blue')

        # add axis labels to plot
        ax.set_title('Precision-Recall Curve')
        ax.set_ylabel('Precision')
        ax.set_xlabel('Recall')

        # display plot
        # plt.show()
        plt.savefig(os.path.join(path_to_save, 'PRC_1{}.pdf'.format(stage)), format="pdf", bbox_inches="tight")
        plt.close('all')

    if path_to_save and stage is not None:  ###is the same as vifore but is also compute the average precision recalls and shos in the plot
        labels_str = np.where(labels == 1, 'COVID Positives', 'COVID Negatives')

        PrecisionRecallDisplay.from_predictions(labels_str, prediction, pos_label='COVID Positives')

        plt.title('Precision-Recall Curve')
        plt.savefig(os.path.join(path_to_save, 'PRC_2{}.pdf'.format(stage)), format="pdf", bbox_inches="tight")
        plt.close('all')

        # display plot
        ###plt.show()

    AVE_PR = average_precision_score(labels, prediction)

    return AVE_PR


def calc_multi_cls_measures(probs, label,cut_off=None, acelerator=None):
    """Calculate multi-class classification measures (Accuracy, precision,
    Recall, AUC.

    :probs: NxC numpy array storing probabilities for each case
    :label: ground truth label
    :returns: a dictionary of accuracy, precision and recall

    """
    # n_classes = probs.shape[1]
    if cut_off is not None:
        preds = np.where(probs[:, 1] >= cut_off, 1, 0)
    else:
        preds = np.argmax(probs, axis=1)
    accuracy = accuracy_score(label, preds)
    # precisions = precision_score(label, preds, average=None,
    #                              labels=range(n_classes), zero_division=0.)
    # recalls = recall_score(label, preds, average=None, labels=range(n_classes),
    #                        zero_division=0.)
    print(classification_report(y_true=label, y_pred=preds, labels=[0, 1],
                                target_names=['NON-COVID', 'COVID']))

    precisions, recalls, fscore, support = precision_recall_fscore_support(y_true=label, labels=[0, 1],
                                                                           y_pred=preds,
                                                                           average=None)

    cnf_matrix = confusion_matrix(label, preds, labels=[0, 1])

    tn, fp, fn, tp = cnf_matrix.ravel()

    convid_sensitivity = recalls[1]

    convid_specificity = recalls[0]

    print(
        'CM | TN: %d | TP: %d | FN: %d | FP: %d\n'
        % (tn, tp, fn, fp))

    AVE_AUC = compute_roc_auc(labels=label, prediction=probs[:, 1])

    pr_score = average_precision_score(label, probs[:,1])

    macro_F1 = f1_score(label, preds, average='macro')

    metric_collects = {'accuracy': accuracy, 'precisions': precisions,
                       'recalls': recalls, 'AUC': AVE_AUC, 'Se': convid_sensitivity,
                       'Sp': convid_specificity, 'macro_f1': macro_F1, 'Ave_pr_score': pr_score}
    return metric_collects

def calc_multi_cls_measures_ECCV2022_Challenge(problem,probs, label,cut_off=None, acelerator=None):
    """Calculate multi-class classification measures (Accuracy, precision,
    Recall, AUC.

    :probs: NxC numpy array storing probabilities for each case
    :label: ground truth label
    :returns: a dictionary of accuracy, precision and recall

    """
    # n_classes = probs.shape[1]

    if problem=='covid_cls':
        labels_idx_array=[0,1]
        target_names_array=['NON-COVID', 'COVID']
        if cut_off is not None:
            preds = np.where(probs[:, 1] >= cut_off, 1, 0)
        else:
            preds = np.argmax(probs, axis=1)
    elif problem=='covid_severity_cls':
        labels_idx_array=[0,1,2,3]
        target_names_array=['Category-1','Category-2','Category-3','Category-4']
        preds = np.argmax(probs, axis=1)

    accuracy = accuracy_score(label, preds)

    print(classification_report(y_true=label, y_pred=preds, labels=labels_idx_array,
                                target_names=target_names_array))

    precisions, recalls, fscore, support = precision_recall_fscore_support(y_true=label, labels=labels_idx_array,
                                                                           y_pred=preds,
                                                                           average=None)

    if problem == 'covid_cls':
        cnf_matrix = confusion_matrix(label, preds, labels=labels_idx_array)

        tn, fp, fn, tp = cnf_matrix.ravel()

        convid_sensitivity = recalls[1]

        convid_specificity = recalls[0]

        print(
            'CM | TN: %d | TP: %d | FN: %d | FP: %d\n'
            % (tn, tp, fn, fp))

        AVE_AUC = compute_roc_auc(labels=label, prediction=probs[:, 1])

        pr_score = average_precision_score(label, probs[:, 1])

        macro_F1 = f1_score(label, preds, average='macro')

        metric_collects = {'accuracy': accuracy, 'precisions': precisions,
                           'recalls': recalls, 'AUC': AVE_AUC, 'Se': convid_sensitivity,
                           'Sp': convid_specificity, 'macro_f1': macro_F1, 'Ave_pr_score': pr_score}
    elif problem == 'covid_severity_cls':

        macro_F1 = f1_score(label, preds, average='macro')

        metric_collects = {'accuracy': accuracy, 'precisions': precisions,
                           'recalls': recalls, 'macro_f1': macro_F1}
    return metric_collects




def print_epoch_metrics(args, epoch, metric_collects, stage, loss,acelerator=None):

    if acelerator is not None:
        acelerator.wait_for_everyone()
    print('%s metrics report at epoch: %03d/%03d: \n' % (stage,epoch, args.num_epochs))
    print(
        '%s metrics: %03d/%03d | Loss: %.5f | Accuraccy: %.4f | Sensitivity: %.4f | Specificity: %.4f | '
        'AVE_F1: %.4f | AUC_AVE: %.4f | PRS_AVE: %4f......\n' % (stage,epoch, args.num_epochs, loss,
                                                                 metric_collects['accuracy'], metric_collects['Se'],
                                                                 metric_collects['Sp'], metric_collects['macro_f1'],
                                                                 metric_collects['AUC'], metric_collects['Ave_pr_score']))

def print_epoch_metrics_ECCV2022_Challenge(args, epoch, metric_collects, stage, loss,acelerator=None):

    if acelerator is not None:
        acelerator.wait_for_everyone()

    if args.cls_problem=='covid_cls':
        print('%s %s metrics report at epoch: %03d/%03d: \n' % (stage, args.cls_problem,epoch, args.num_epochs))
        print(
            '%s %s metrics: %03d/%03d | Loss: %.5f | Accuraccy: %.4f | Sensitivity: %.4f | Specificity: %.4f | '
            'AVE_F1: %.4f | AUC_AVE: %.4f | PRS_AVE: %4f......\n' % (stage, args.cls_problem,epoch, args.num_epochs, loss,
                                                                     metric_collects['accuracy'], metric_collects['Se'],
                                                                     metric_collects['Sp'], metric_collects['macro_f1'],
                                                                     metric_collects['AUC'], metric_collects['Ave_pr_score']))
    elif args.cls_problem == 'covid_severity_cls':
        print('%s %s metrics report at epoch: %03d/%03d: \n' % (stage, args.cls_problem, epoch, args.num_epochs))
        print(
            '%s %s metrics: %03d/%03d | Loss: %.5f | Accuraccy: %.4f AVE_F1: %.4f......\n' %
                                                                    (stage, args.cls_problem, epoch, args.num_epochs, loss,
                                                                     metric_collects['accuracy'],
                                                                     metric_collects['macro_f1']))

def make_confusion_matrix(cf,
                          path_to_save,
                          stage,
                          group_names=None,
                          categories='auto',
                          count=True,
                          percent=True,
                          cbar=True,
                          xyticks=True,
                          xyplotlabels=True,
                          sum_stats=True,
                          figsize=None,
                          cmap='Blues',
                          title=None):
    '''
    This function will make a pretty plot of an sklearn Confusion Matrix cm using a Seaborn heatmap visualization.
    Arguments
    ---------
    cf:            confusion matrix to be passed in
    group_names:   List of strings that represent the labels row by row to be shown in each square.
    categories:    List of strings containing the categories to be displayed on the x,y axis. Default is 'auto'
    count:         If True, show the raw number in the confusion matrix. Default is True.
    normalize:     If True, show the proportions for each category. Default is True.
    cbar:          If True, show the color bar. The cbar values are based off the values in the confusion matrix.
                   Default is True.
    xyticks:       If True, show x and y ticks. Default is True.
    xyplotlabels:  If True, show 'True Label' and 'Predicted Label' on the figure. Default is True.
    sum_stats:     If True, display summary statistics below the figure. Default is True.
    figsize:       Tuple representing the figure size. Default will be the matplotlib rcParams value.
    cmap:          Colormap of the values displayed from matplotlib.pyplot.cm. Default is 'Blues'
                   See http://matplotlib.org/examples/color/colormaps_reference.html

    title:         Title for the heatmap. Default is None.
    '''

    # CODE TO GENERATE TEXT INSIDE EACH SQUARE
    blanks = ['' for i in range(cf.size)]

    if group_names and len(group_names) == cf.size:
        group_labels = ["{}\n".format(value) for value in group_names]
    else:
        group_labels = blanks

    if count:
        group_counts = ["{0:0.0f}\n".format(value) for value in cf.flatten()]
    else:
        group_counts = blanks

    if percent:
        group_percentages = ["{0:.2%}".format(value) for value in cf.flatten() / np.sum(cf)]
    else:
        group_percentages = blanks

    box_labels = [f"{v1}{v2}{v3}".strip() for v1, v2, v3 in zip(group_labels, group_counts, group_percentages)]
    box_labels = np.asarray(box_labels).reshape(cf.shape[0], cf.shape[1])

    # CODE TO GENERATE SUMMARY STATISTICS & TEXT FOR SUMMARY STATS
    if sum_stats:
        # Accuracy is sum of diagonal divided by total observations
        accuracy = np.trace(cf) / float(np.sum(cf))

        # if it is a binary confusion matrix, show some more stats
        if len(cf) == 2:
            # Metrics for Binary Confusion Matrices
            precision = cf[1, 1] / sum(cf[:, 1])
            recall = cf[1, 1] / sum(cf[1, :])
            f1_score = 2 * precision * recall / (precision + recall)
            stats_text = "\n\nAccuracy={:0.3f}\nPrecision={:0.3f}\nRecall={:0.3f}\nF1 Score={:0.3f}".format(
                accuracy, precision, recall, f1_score)
        else:
            stats_text = "\n\nAccuracy={:0.3f}".format(accuracy)
    else:
        stats_text = ""

    # SET FIGURE PARAMETERS ACCORDING TO OTHER ARGUMENTS
    if figsize == None:
        # Get default figure size if not set
        figsize = plt.rcParams.get('figure.figsize')

    if xyticks == False:
        # Do not show categories if xyticks is False
        categories = False

    # MAKE THE HEATMAP VISUALIZATION
    plt.figure(figsize=figsize)
    sns.heatmap(cf, annot=box_labels, fmt="", cmap=cmap, cbar=cbar, xticklabels=categories, yticklabels=categories)

    if xyplotlabels:
        plt.ylabel('Ground Truth')
        plt.xlabel('Model Predictions' + stats_text)
    else:
        plt.xlabel(stats_text)

    if title:
        plt.title(title)

    # plt.savefig('CF.pdf',format="pdf")
    # plt.show()
    plt.savefig(os.path.join(path_to_save, 'CFM_{}.pdf'.format(stage)), format='pdf', bbox_inches='tight')
    plt.close('all')
