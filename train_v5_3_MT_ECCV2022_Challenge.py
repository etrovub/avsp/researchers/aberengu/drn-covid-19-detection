##########################################################################################
# (c) Copyright 2022
# The author(s):    Abel Díaz Berenguer
#                   Tanmoy Mukherjee
# Vrije Universiteit Brussel,
# Department of Electronics and Informatics (ETRO),
# Faculty of Engineering Sciences,
# VUB-ETRO,
# 2 Pleinlaan, 1050 Belgium.
# (VUB - ETRO)
#
# All right reserved
# Permission to use, copy, modify, and distribute within VUB-ETRO
# this routine/program/software and/or its documentation, or part of any one of them
# (the program) for research purpose within VUB-ETRO is hereby granted, provided:
# (1) that the copyright notice appears in all copies,
# (2) that both the copyright notice and this permission notice appear in the supporting documentation, and
# (3) that all publications based partially or completely on this program will have the main publications of "the authors" related to this
# work cited as references.
# Any other type of handling or use of the program, CAN NOT BE DONE
# WITHOUT PRIOR WRITTEN APPROVAL from the VUB-ETRO,
# including but not restricted to the following:
# (A) any re-distribution of the program to any person outside of the VUB-ETRO
# (B) any use of the program which is not academic research;
# (C) any commercial activity involving the use of the program is strictly prohibited.
# Modifications: 2018
# Disclaimer: VUB ETRO make no claim about the
# suitability of the program for any particular purpose.
# It is provided "as is" without any form of warranty or support.
##########################################################################################
import numpy as np
import torch.nn.functional as F
import torch.nn as nn
import torch
import time
from datetime import datetime
import os
import utils
from torch.utils.tensorboard import SummaryWriter
import wandb
from einops import rearrange, repeat
from timm.utils import *

from timm.models import model_parameters

from timm.utils import get_state_dict

import monai
from monai.transforms import RandAdjustContrast, ToTensor, RandGaussianSmooth, RandHistogramShift, RandZoom, RandShiftIntensity

from copy import deepcopy

#todo
NO_LABEL=-1
import losses
import ramps

try:
    # noinspection PyUnresolvedReferences
    from apex import amp

except ImportError:
    amp = None

##########################
### SETTINGS
##########################
# RANDOM_SEED = 42
# # ####TODO correctly
# if torch.cuda.is_available():
#     torch.backends.cudnn.deterministic = True
# #Device
# device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
##########################
### Train the model
##########################
def get_lr(optimizer):
    for param_group in optimizer.param_groups:
        return param_group['lr']

def get_current_consistency_weight(args, epoch):
    # Consistency ramp-up from https://arxiv.org/abs/1610.02242
    return args.consistency_weight * ramps.sigmoid_rampup(epoch, args.consistency_weight_rampup)

def get_current_proj_distance_weight(args, epoch):
    # Consistency ramp-up from https://arxiv.org/abs/1610.02242
    return args.proj_distance_weight * ramps.sigmoid_rampup(epoch, args.distance_weight_rampup)

class GaussianNoise(nn.Module):

    def __init__(self, batch_size, input_shape=(1, 3, 224, 224,112), std=0.05, device='cuda'):
        super(GaussianNoise, self).__init__()
        self.shape = (batch_size,) + input_shape
        self.noise = torch.autograd.Variable(torch.zeros(self.shape)).to(device)
        self.std = std

    def forward(self, x):
        self.noise.data.normal_(0, std=self.std)
        return x + self.noise

def train_model(args, model, optimizer, train_loader, val_loader, num_training_steps_per_epoch,criterion, model_ema=None):

    device=args.device

    if args.consistency_type == 'mse':
        consistency_criterion = losses.softmax_mse_loss
    elif args.consistency_type == 'kl':
        consistency_criterion = losses.softmax_kl_loss
    else:
        assert False, args.consistency_type

    residual_logit_criterion = losses.symmetric_mse_loss


    if args.proj_distance_type == 'mse':
        projection_criterion = losses.proj_loss
    elif args.proj_distance_type == 'kl':
        projection_criterion= losses.softmax_kl_loss
    elif args.proj_distance_type == 'smis':
        projection_criterion = losses.smisoft
    elif args.proj_distance_type == 'smi':
        projection_criterion = losses.smi
    elif args.proj_distance_type == 'infomax':
        projection_criterion = losses.getMutualInformation
    elif args.proj_distance_type == 'jsd':
        projection_criterion = losses.softmax_JSD_loss


    writer = SummaryWriter(log_dir=args.checkpoint_output)

    os.environ["WANDB_API_KEY"] = 'TBD'

    wandb.login()

    if args.semi_supervised:
        ssl_name='SSL ' + args.cls_problem
        if args.ratio_unlabeled>0:
            ratio_unlab_name=args.ratio_unlabeled*100
        else:
            ratio_unlab_name = 0


        more_info=datetime.now().strftime(" Date: %m/%d/%Y Start_Time: %H:%M:%S ")+ssl_name+' Unlab ratio: {}'.format(ratio_unlab_name)
    else:
        ssl_name='SUP '+ args.cls_problem
        more_info= datetime.now().strftime(" Date: %m/%d/%Y Start_Time: %H:%M:%S ")+ssl_name

    # Initialize WandB
    config = args.__dict__
    wandb.init(name='TBD '+ more_info,
               project='TBD',
               group='TBD',
               entity='TBD',
               config=config,
               tags=['ssl_name ',args.model, args.proj_distance_type,args.consistency_type])

    wandb.watch(model)

    if model_ema is not None:
        wandb.watch(model_ema.module)

    print("Use Cosine LR scheduler")
    lr_schedule_values = None
    lr_schedule_values = utils.cosine_scheduler(
        args.lr, args.min_lr, args.num_epochs, num_training_steps_per_epoch,
        warmup_epochs=args.warmup_epochs, warmup_steps=args.warmup_steps,
    )
    print("Use Cosine LR scheduler for wd")
    wd_schedule_values = None
    if args.weight_decay_end is None:
        args.weight_decay_end = args.weight_decay
    wd_schedule_values = utils.cosine_scheduler(
        args.weight_decay, args.weight_decay_end, args.num_epochs, num_training_steps_per_epoch)
    print("Max WD = %.7f, Min WD = %.7f" % (max(wd_schedule_values), min(wd_schedule_values)))

    ### Checkpoint
    checkpoint = {
        'counters': {
            'epoch': None,
            'best_epoch': None,
        },
        'state': None,
        'ema_state': None,
        'optim_state': None,
    }

    if args.cls_problem=='covid_cls':
        train_sensitivity_list = []
        val_sensitivity_list = []

        train_specificity_list = []
        val_specificity_list = []

        train_AUC_list = []
        val_AUC_list = []

        train_PRS_list = []
        val_PRS_list = []

    train_loss_list = []
    val_loss_list = []

    train_accuracy_list = []
    val_accuracy_list = []

    train_macro_F1_list = []
    val_macro_F1_list = []

    if model_ema is not None:

        if args.cls_problem == 'covid_cls':
            train_sensitivity_list_model_ema = []
            val_sensitivity_list_model_ema = []



            train_specificity_list_model_ema = []
            val_specificity_list_model_ema = []

            train_AUC_list_model_ema = []
            val_AUC_list_model_ema = []

            train_PRS_list_model_ema = []
            val_PRS_list_model_ema = []


        train_loss_list_model_ema = []

        train_accuracy_list_model_ema = []

        train_macro_F1_list_model_ema = []

        val_loss_list_model_ema = []

        val_accuracy_list_model_ema = []

        val_macro_F1_list_model_ema = []

    start_time = time.time()

    ###Validation options
    if args.cls_problem == 'covid_cls':

        max_val_ave_auc = 0

        max_val_pr_score = 0


    max_val_macro_F1 = 0

    val_early_stop = 0

    ##Start_training...
    start_epoch = 0
    if args.start_epoch is not None:
        start_epoch = args.start_epoch

    update_freq = args.update_freq

    if args.proj_distance_weight >= 0:

        RANDOM_AUGMENT=monai.transforms.Compose([


                RandGaussianSmooth(sigma_x=(0.5, 1.15),
                                   sigma_y=(0.5, 1.15),
                                   sigma_z=(0.5, 1.15),
                                   prob=0.8),


                RandAdjustContrast(prob=0.8,
                                   gamma=(0.5, 4.5)
                                   ),

                RandZoom(min_zoom=0.4,
                         max_zoom=0.8,
                         prob=0.8),

                RandShiftIntensity(
                    offsets=0.20,
                    prob=0.8),

                RandHistogramShift(num_control_points=(3, 10),
                                   prob=0.8,),

                ToTensor()
            ])

    for epoch in range(start_epoch, args.num_epochs):
        model.train(True)
        epoch_loss = 0
        num_examples = 0
        correct_pred_model = 0
        epoch_class_loss_model = 0

        if model_ema is not None:
            epoch_proj_loss = 0
            epoch_res_loss = 0
            epoch_class_loss_ema = 0
            epoch_cons_loss = 0
            correct_pred_ema = 0

        start_time_epoch = time.time()
        start_steps = epoch * num_training_steps_per_epoch

        ###num_updates = epoch * len(train_loader)
        for batch_idx, batch_data in enumerate(train_loader):

            step = batch_idx // update_freq
            if step >= num_training_steps_per_epoch:
                continue
            it = start_steps + step  # global training iteration

            if lr_schedule_values is not None or wd_schedule_values is not None and batch_idx % update_freq == 0:
                for i, param_group in enumerate(optimizer.param_groups):
                    if lr_schedule_values is not None:
                        param_group["lr"] = lr_schedule_values[it] * param_group["lr_scale"]
                    if wd_schedule_values is not None and param_group["weight_decay"] > 0:
                        param_group["weight_decay"] = wd_schedule_values[it]
            inputs, targets = batch_data['image_ct'].to(device), batch_data['label'].to(device)

            inputs = repeat(inputs, 'b c h w d ->b (repeat c) h w d ',
                            repeat=3)  ##we use this to keep 3 channels input for pretrained models
            inputs = rearrange(inputs, 'b c h w d -> b c d h w')

            if args.proj_distance_weight >= 0:
                ###allocate mem
                img_one = torch.zeros(inputs.shape, device=device)
                img_two = torch.zeros(inputs.shape, device=device)
                while (img_one == img_two).sum()>0: ###Ensure the two views are different
                    for i in range(inputs.shape[0]):
                        img_one[i, ...] = RANDOM_AUGMENT(inputs[i, ...])
                        img_two[i, ...] = RANDOM_AUGMENT(inputs[i, ...])

            # outputs = model(inputs)
            #####from https://github.com/CuriousAI/mean-teacher/blob/546348ff863c998c26be4339021425df973b4a36/pytorch/main.py#L383 Adapted to our project
            ##->>>>>
                input_var = torch.autograd.Variable(img_one)
                ema_input_var = torch.autograd.Variable(img_two)
            else:
                input_var = torch.autograd.Variable(inputs)
                ema_input_var = torch.autograd.Variable(inputs)

            target_var = torch.autograd.Variable(targets)

            minibatch_size = len(target_var)

            model_out = model(input_var)

            if model_ema is not None:
                with torch.no_grad():
                    ema_model_out = model_ema.module(ema_input_var)

            if args.proj_distance_weight >= 0:
                assert len(model_out) == 2
                assert len(ema_model_out) == 2
                logit1 = model_out[0]
                proj_model = model_out[1]
                if args.logit_distance_weight > 0:
                    logit2 = F.softmax(logit1, dim=1)
                ema_logit = torch.autograd.Variable(ema_model_out[0].detach().data, requires_grad=False)
                proj_ema = torch.autograd.Variable(ema_model_out[1].detach().data, requires_grad=False)
            else:
                logit1 = model_out
                if args.logit_distance_weight > 0:
                    logit2 = F.softmax(logit1, dim=1)
                ema_logit = torch.autograd.Variable(ema_model_out.detach().data, requires_grad=False)

            if model_ema is not None:

                if args.proj_distance_weight >= 0:  ##thi is the loss of the student logit before and after softmax...
                    proj_distance_weight = get_current_proj_distance_weight(args, epoch)
                    proj_loss = proj_distance_weight * projection_criterion(proj_model, proj_ema) / minibatch_size
                    if torch.isnan(proj_loss):
                        proj_loss=0.0
                    elif proj_loss.item() > 1e5:
                        proj_loss = 0.05
                    # meters.update('res_loss', res_loss.data[0])
                    # writer.add_scalar("proj_loss", proj_loss.item(), it)
                    writer.add_scalar("proj_weight", proj_distance_weight, it)
                    wandb.log({
                        "proj_weight": proj_distance_weight
                    })
                    epoch_proj_loss += proj_loss
                else:
                    proj_loss = 0

            if args.logit_distance_weight > 0:  ##thi is the loss of the student logit before and after softmax...
                class_logit, cons_logit = logit1, logit2  ##this i'm not sure if necesary
                res_loss = args.logit_distance_weight * residual_logit_criterion(class_logit,
                                                                                 cons_logit)
                # writer.add_scalar("res_loss", res_loss.item(), it)
                epoch_res_loss += res_loss
            else:
                class_logit, cons_logit = logit1, logit1
                res_loss = 0

            class_loss_model = criterion(class_logit, target_var.long())
            epoch_class_loss_model += class_loss_model

            if model_ema is not None:
                class_loss_ema = criterion(ema_logit, target_var.long())
                epoch_class_loss_ema += class_loss_ema

            if model_ema is not None:
                if args.consistency_weight >= 0:
                    consistency_weight = get_current_consistency_weight(args, epoch)
                    writer.add_scalar("cons_weight", consistency_weight, it)
                    wandb.log({
                        "cons_weight": consistency_weight
                    })
                    consistency_loss = consistency_weight * consistency_criterion(cons_logit, ema_logit) / minibatch_size
                    epoch_cons_loss += consistency_loss
                else:
                    consistency_loss = 0

            batch_loss = class_loss_model + consistency_loss + res_loss + proj_loss

            assert not (np.isnan(batch_loss.item()) or batch_loss.item() > 1e5), 'Loss explosion: {}' \
                .format(batch_loss.item())

            batch_loss /= update_freq
            batch_loss.backward()
            if (batch_idx + 1) % update_freq == 0:
                optimizer.step()
                if args.clip_grad is not None:
                    dispatch_clip_grad(
                        model_parameters(model, exclude_head='agc' in args.clip_mode),
                        value=args.clip_grad, mode=args.clip_mode)
                optimizer.zero_grad()
                if model_ema is not None:
                    model_ema.update(model)
            min_lr = 10.
            max_lr = 0.
            for group in optimizer.param_groups:
                min_lr = min(min_lr, group["lr"])
                max_lr = max(max_lr, group["lr"])

            for group in optimizer.param_groups:
                if group["weight_decay"] > 0:
                    weight_decay_value = group["weight_decay"]

            epoch_loss += batch_loss

            probas_model = F.softmax(logit1, dim=1)

            predicted_labels_model = logit1.argmax(dim=1)

            probas_ema = F.softmax(ema_logit, dim=1)

            predicted_labels_ema = ema_logit.argmax(dim=1)

            num_examples += len(targets)

            correct_pred_model += torch.eq(predicted_labels_model, targets).sum().item()

            if model_ema is not None:
                correct_pred_ema += torch.eq(predicted_labels_ema, targets).sum().item()

            writer.add_scalar("learning_rate", max_lr, it)
            writer.add_scalar("weight_decay", weight_decay_value, it)

            wandb.log({
                "learning_rate": max_lr,
                "weight_decay": weight_decay_value
            })

            if batch_idx == 0:
                scores = probas_model.detach().cpu().numpy()
                labels = targets.detach().cpu().numpy()
            else:
                scores = np.concatenate((scores, probas_model.detach().cpu().numpy()), axis=0)
                labels = np.concatenate((labels, targets.detach().cpu().numpy()))

            if model_ema is not None:
                if batch_idx == 0:
                    scores_ema = probas_ema.detach().cpu().numpy()
                else:
                    scores_ema = np.concatenate((scores_ema, probas_ema.detach().cpu().numpy()), axis=0)

        train_loss = epoch_loss / num_examples
        writer.add_scalar('Train_Total_Loss', train_loss, epoch + 1)
        wandb.log({
            "Train_Total_Loss": train_loss})


        train_class_loss_model = epoch_class_loss_model / num_examples

        if model_ema is not None:

            train_class_loss_ema = epoch_class_loss_ema / num_examples

        if args.ratio_unlabeled > 0.0 and args.semi_supervised:
            scores=scores[np.where(labels!=-1)]
            labels=labels[np.where(labels!=-1)]

        metric_collects_train = utils.calc_multi_cls_measures_ECCV2022_Challenge(args.cls_problem,scores, labels)

        if args.cls_problem=='covid_cls':

            writer.add_scalar('Train_Class_Loss_model_ema_{}'.format(str(False)), train_class_loss_model, epoch + 1)
            writer.add_scalar('Train_ACC_model_ema_{}'.format(str(False)), metric_collects_train['accuracy'], epoch + 1)
            writer.add_scalar('Train_AUC_model_ema_{}'.format(str(False)), metric_collects_train['AUC'], epoch + 1)
            writer.add_scalar('Train_macro_f1_model_ema_{}'.format(str(False)), metric_collects_train['macro_f1'], epoch + 1)
            writer.add_scalar('Train_AVE_Pr_score_model_ema_{}'.format(str(False)), metric_collects_train['Ave_pr_score'], epoch + 1)

            wandb.log({
                "Train_Class_Loss_model_ema_{}".format(str(False)): train_class_loss_model,
                "Train_ACC_model_ema_{}".format(str(False)): metric_collects_train['accuracy'],
                "Train_AUC_model_ema_{}".format(str(False)): metric_collects_train['AUC'],
                "Train_macro_f1_model_ema_{}".format(str(False)): metric_collects_train['macro_f1'],
                "Train_AVE_Pr_score_model_ema_{}".format(str(False)): metric_collects_train['Ave_pr_score']
            })
        elif args.cls_problem=='covid_severity_cls':

            writer.add_scalar('Train_Class_Loss_model_ema_{}'.format(str(False)), train_class_loss_model, epoch + 1)
            writer.add_scalar('Train_ACC_model_ema_{}'.format(str(False)), metric_collects_train['accuracy'], epoch + 1)
            writer.add_scalar('Train_macro_f1_model_ema_{}'.format(str(False)), metric_collects_train['macro_f1'],
                              epoch + 1)

            wandb.log({
                "Train_Class_Loss_model_ema_{}".format(str(False)): train_class_loss_model,
                "Train_ACC_model_ema_{}".format(str(False)): metric_collects_train['accuracy'],
                "Train_macro_f1_model_ema_{}".format(str(False)): metric_collects_train['macro_f1']
            })

        utils.print_epoch_metrics_ECCV2022_Challenge(args,epoch=epoch+1,metric_collects=metric_collects_train,stage='Training',loss=train_loss.item())

        if model_ema is not None:
            if args.ratio_unlabeled > 0.0 and args.semi_supervised:
                scores_ema = scores[np.where(labels != -1)]
            metric_collects_train_model_ema = utils.calc_multi_cls_measures_ECCV2022_Challenge(args.cls_problem, scores_ema, labels)

            if args.cls_problem == 'covid_cls':

                writer.add_scalar('Train_Class_Loss_model_ema_{}'.format(str(True)), train_class_loss_ema, epoch + 1)
                writer.add_scalar('Train_ACC_model_ema_{}'.format(str(True)), metric_collects_train_model_ema['accuracy'], epoch + 1)
                writer.add_scalar('Train_AUC_model_ema_{}'.format(str(True)), metric_collects_train_model_ema['AUC'], epoch + 1)
                writer.add_scalar('Train_macro_f1_model_ema_{}'.format(str(True)), metric_collects_train_model_ema['macro_f1'], epoch + 1)
                writer.add_scalar('Train_AVE_Pr_score_model_ema_{}'.format(str(True)), metric_collects_train_model_ema['Ave_pr_score'], epoch + 1)

                wandb.log({
                    "Train_Class_Loss_model_ema_{}".format(str(True)): train_class_loss_ema,
                    "Train_ACC_model_ema_{}".format(str(True)): metric_collects_train_model_ema['accuracy'],
                    "Train_AUC_model_ema_{}".format(str(True)): metric_collects_train_model_ema['AUC'],
                    "Train_macro_f1_model_ema_{}".format(str(True)): metric_collects_train_model_ema['macro_f1'],
                    "Train_AVE_Pr_score_model_ema_{}".format(str(True)): metric_collects_train_model_ema['Ave_pr_score']
                })

            elif args.cls_problem == 'covid_severity_cls':
                writer.add_scalar('Train_Class_Loss_model_ema_{}'.format(str(True)), train_class_loss_ema, epoch + 1)
                writer.add_scalar('Train_ACC_model_ema_{}'.format(str(True)),
                                  metric_collects_train_model_ema['accuracy'], epoch + 1)
                writer.add_scalar('Train_macro_f1_model_ema_{}'.format(str(True)),
                                  metric_collects_train_model_ema['macro_f1'], epoch + 1)
                wandb.log({
                    "Train_Class_Loss_model_ema_{}".format(str(True)): train_class_loss_ema,
                    "Train_ACC_model_ema_{}".format(str(True)): metric_collects_train_model_ema['accuracy'],
                    "Train_macro_f1_model_ema_{}".format(str(True)): metric_collects_train_model_ema['macro_f1']
                })


        if epoch % args.check_every == 0 and epoch > 1:
            ###Validate model
            if (epoch) % args.check_every == 0 and epoch > 1:

                metric_collects_val, val_loss = \
                    validate_model(args,epoch,model,val_loader,criterion,writer)

                if args.cls_problem=='covid_cls':

                    train_AUC_list.append(metric_collects_train['AUC'])
                    val_AUC_list.append(metric_collects_val['AUC'])

                    train_sensitivity_list.append(metric_collects_train['Se'])
                    val_sensitivity_list.append(metric_collects_val['Se'])

                    train_specificity_list.append(metric_collects_train['Sp'])
                    val_specificity_list.append(metric_collects_val['Sp'])

                    train_PRS_list.append(metric_collects_train['Ave_pr_score'])
                    val_PRS_list.append(metric_collects_val['Ave_pr_score'])

                train_loss_list.append(train_loss.item())
                val_loss_list.append(val_loss.item())

                train_accuracy_list.append(metric_collects_train['accuracy'])
                val_accuracy_list.append(metric_collects_val['accuracy'])

                train_macro_F1_list.append(metric_collects_train['macro_f1'])
                val_macro_F1_list.append(metric_collects_val['macro_f1'])

                if model_ema is not None:

                    if args.cls_problem == 'covid_cls':
                        train_AUC_list_model_ema.append(metric_collects_train_model_ema['AUC'])

                        train_sensitivity_list_model_ema.append(metric_collects_train_model_ema['Se'])

                        train_specificity_list_model_ema.append(metric_collects_train_model_ema['Sp'])

                        train_PRS_list_model_ema.append(metric_collects_train_model_ema['Ave_pr_score'])

                    train_loss_list_model_ema.append(train_class_loss_ema.item())

                    train_accuracy_list_model_ema.append(metric_collects_train_model_ema['accuracy'])

                    train_macro_F1_list_model_ema.append(metric_collects_train_model_ema['macro_f1'])

                    metric_collects_val_model_ema, val_loss_model_ema = \
                        validate_model(args, epoch, model_ema.module, val_loader, criterion, writer, is_model_ema=True)

                    if args.cls_problem == 'covid_cls':

                        val_AUC_list_model_ema.append(metric_collects_val_model_ema['AUC'])

                        val_sensitivity_list_model_ema.append(metric_collects_val_model_ema['Se'])

                        val_specificity_list_model_ema.append(metric_collects_val_model_ema['Sp'])

                        val_PRS_list_model_ema.append(metric_collects_val_model_ema['Ave_pr_score'])

                    val_loss_list_model_ema.append(val_loss_model_ema.item())

                    val_accuracy_list_model_ema.append(metric_collects_val_model_ema['accuracy'])

                    val_macro_F1_list_model_ema.append(metric_collects_val_model_ema['macro_f1'])
                ###Save the model
                val_early_stop += 1

                if args.cls_problem=='covid_cls':

                    if model_ema is not None:
                        val_ave_auc= max(metric_collects_val['AUC'],metric_collects_val_model_ema['AUC'] )
                        val_pr_score = max(metric_collects_val['Ave_pr_score'] , metric_collects_val_model_ema['Ave_pr_score'])
                        val_macro_F1 = max(metric_collects_val['macro_f1'], metric_collects_val_model_ema['macro_f1'])
                    else:
                        val_ave_auc = metric_collects_val['AUC']
                        val_pr_score = metric_collects_val['Ave_pr_score']
                        val_macro_F1 = metric_collects_val['macro_f1']


                    if val_ave_auc > max_val_ave_auc or val_pr_score > max_val_pr_score or val_macro_F1 > max_val_macro_F1:

                        val_early_stop = 0

                        if val_ave_auc > max_val_ave_auc:
                            max_val_ave_auc = val_ave_auc
                            print('New Higher Validation AVE_AUC at Epoch: %03d/%03d | Maximun AVE_AUC: %.4f...\n'
                                  % (epoch+1, args.num_epochs, max_val_ave_auc))
                        if val_pr_score > max_val_pr_score:
                            max_val_pr_score = val_pr_score
                            print('New Higher Validation PR score at Epoch: %03d/%03d | '
                                  'Maximun PR score: %.4f...\n'
                                  % (epoch+1, args.num_epochs, max_val_pr_score))
                        if val_macro_F1 > max_val_macro_F1:
                            max_val_macro_F1 = val_macro_F1
                            print('New Higher Validation AVE F1 score at Epoch: %03d/%03d | '
                                  'Maximun AVE F1 score: %.4f...\n'
                                  % (epoch+1, args.num_epochs, max_val_macro_F1))
                        checkpoint['counters']['epoch'] = epoch+1
                        checkpoint['state'] = model.state_dict()
                        checkpoint['optim_state'] = optimizer.state_dict()
                        if model_ema is not None:
                            checkpoint['ema_state_state'] =get_state_dict(model_ema)
                        checkpoint_path = os.path.join(
                            args.checkpoint_output, 'model_val_epoch_{}.pt'.format((epoch+1))
                        )
                        print('Saving checkpoint to {}...\n'.format(checkpoint_path))
                        torch.save(checkpoint, checkpoint_path)
                    if val_early_stop == args.num_epochs_val_early_stop:
                        print(
                            'Early Stop at Epoch: %03d/%03d because of now improvements in validation after %03d Epochs...\n'
                            % (epoch+1, args.num_epochs, args.num_epochs_val_early_stop))
                        break
                elif args.cls_problem == 'covid_severity_cls':
                    if model_ema is not None:
                        val_macro_F1 = max(metric_collects_val['macro_f1'], metric_collects_val_model_ema['macro_f1'])
                    else:
                        val_macro_F1 = metric_collects_val['macro_f1']

                    if val_macro_F1 > max_val_macro_F1:

                        val_early_stop = 0

                        if val_macro_F1 > max_val_macro_F1:
                            max_val_macro_F1 = val_macro_F1
                            print('New Higher Validation AVE F1 score at Epoch: %03d/%03d | '
                                  'Maximun AVE F1 score: %.4f...\n'
                                  % (epoch + 1, args.num_epochs, max_val_macro_F1))
                        checkpoint['counters']['epoch'] = epoch + 1
                        checkpoint['state'] = model.state_dict()
                        checkpoint['optim_state'] = optimizer.state_dict()
                        if model_ema is not None:
                            checkpoint['ema_state_state'] = get_state_dict(model_ema)
                        checkpoint_path = os.path.join(
                            args.checkpoint_output, 'model_val_epoch_{}.pt'.format((epoch + 1))
                        )
                        print('Saving checkpoint to {}...\n'.format(checkpoint_path))
                        torch.save(checkpoint, checkpoint_path)
                    if val_early_stop == args.num_epochs_val_early_stop:
                        print(
                            'Early Stop at Epoch: %03d/%03d because of now improvements in validation after %03d Epochs...\n'
                            % (epoch + 1, args.num_epochs, args.num_epochs_val_early_stop))
                        break

        print('Time elapsed for Epoch: %03d was : %.2f min...\n' % (epoch+1, (time.time() - start_time_epoch) / 60))

    # report the final metrics to wandb##todo report the final values to wandb
    if args.cls_problem=='covid_cls':
        wandb.run.summary["Best Train ACC model"] = max(train_accuracy_list)
        wandb.run.summary["Best Train AUC model"] = max(train_AUC_list)
        wandb.run.summary["Best Train Macro_F1 model"] = max(train_macro_F1_list)
        wandb.run.summary["Best Train AVE_PRS model"] = max(train_PRS_list)
        ### run summary in case that model ema is being wahtchced
        if model_ema is not None:
            wandb.run.summary["Best Train ACC model_EMA"] = max(train_accuracy_list_model_ema)
            wandb.run.summary["Best Train AUC model_EMA"] = max(train_AUC_list_model_ema),
            wandb.run.summary["Best Train Macro_F1 model_EMA"] = max(train_macro_F1_list_model_ema)
            wandb.run.summary["Best Train AVE_PRS model_EMA"] = max(train_PRS_list_model_ema)
        ##report to wandb the sumary of validation metrics
        wandb.run.summary["Best Val ACC model"] = max(val_accuracy_list)
        wandb.run.summary["Best Val AUC model"] = max(val_AUC_list)
        wandb.run.summary["Best Val Macro_F1 model"] = max(val_macro_F1_list)
        wandb.run.summary["Best Val AVE_PRS model"] = max(val_PRS_list)
        ### run summary in case that model ema is being wahtchced
        if model_ema is not None:
            wandb.run.summary["Best Val ACC model_EMA"] = max(val_accuracy_list_model_ema)
            wandb.run.summary["Best Val AUC model_EMA"] = max(val_AUC_list_model_ema)
            wandb.run.summary["Best Val Macro_F1 model_EMA"] = max(val_macro_F1_list_model_ema)
            wandb.run.summary["Best Val AVE_PRS model_EMA"] = max(val_PRS_list_model_ema)
        wandb.run.summary["state"] = "completed"
        wandb.finish(quiet=True)
    elif args.cls_problem == 'covid_severity_cls':
        wandb.run.summary["Best Train ACC model"] = max(train_accuracy_list)
        wandb.run.summary["Best Train Macro_F1 model"] = max(train_macro_F1_list)
        ### run summary in case that model ema is being wahtchced
        if model_ema is not None:
            wandb.run.summary["Best Train ACC model_EMA"] = max(train_accuracy_list_model_ema)
            wandb.run.summary["Best Train Macro_F1 model_EMA"] = max(train_macro_F1_list_model_ema)
        ##report to wandb the sumary of validation metrics
        wandb.run.summary["Best Val ACC model"] = max(val_accuracy_list)
        wandb.run.summary["Best Val Macro_F1 model"] = max(val_macro_F1_list)
        ### run summary in case that model ema is being wahtchced
        if model_ema is not None:
            wandb.run.summary["Best Val ACC model_EMA"] = max(val_accuracy_list_model_ema)
            wandb.run.summary["Best Val Macro_F1 model_EMA"] = max(val_macro_F1_list_model_ema)
        wandb.run.summary["state"] = "completed"
        wandb.finish(quiet=True)

    if args.cls_problem == 'covid_cls':
        with open(os.path.join(args.checkpoint_output, 'train_sensitivity.npy'), 'w') as outfile:
            np.save(outfile.name, np.asarray(train_sensitivity_list))

        with open(os.path.join(args.checkpoint_output, 'val_sensitivity.npy'), 'w') as outfile:
            np.save(outfile.name, np.asarray(val_sensitivity_list))

        with open(os.path.join(args.checkpoint_output, 'train_specificity.npy'), 'w') as outfile:
            np.save(outfile.name, np.asarray(train_specificity_list))

        with open(os.path.join(args.checkpoint_output, 'val_specificity.npy'), 'w') as outfile:
            np.save(outfile.name, np.asarray(val_specificity_list))

        with open(os.path.join(args.checkpoint_output, 'train_auc.npy'), 'w') as outfile:
            np.save(outfile.name, np.asarray(train_AUC_list))

        with open(os.path.join(args.checkpoint_output, 'val_auc.npy'), 'w') as outfile:
            np.save(outfile.name, np.asarray(val_AUC_list))

        with open(os.path.join(args.checkpoint_output, 'train_pr_score.npy'), 'w') as outfile:
            np.save(outfile.name, np.asarray(train_PRS_list))

        with open(os.path.join(args.checkpoint_output, 'val_pr_score.npy'), 'w') as outfile:
            np.save(outfile.name, np.asarray(val_PRS_list))

    with open(os.path.join(args.checkpoint_output, 'train_acc.npy'), 'w') as outfile:
        np.save(outfile.name, np.asarray(train_accuracy_list))

    with open(os.path.join(args.checkpoint_output, 'val_acc.npy'), 'w') as outfile:
        np.save(outfile.name, np.asarray(val_accuracy_list))

    with open(os.path.join(args.checkpoint_output, 'train_loss.npy'), 'w') as outfile:
        np.save(outfile.name, np.asarray(train_loss_list))

    with open(os.path.join(args.checkpoint_output, 'val_loss.npy'), 'w') as outfile:
        np.save(outfile.name, np.asarray(val_loss_list))

    with open(os.path.join(args.checkpoint_output, 'train_f1_score.npy'), 'w') as outfile:
        np.save(outfile.name, np.asarray(train_macro_F1_list))

    with open(os.path.join(args.checkpoint_output, 'val_f1_score.npy'), 'w') as outfile:
        np.save(outfile.name, np.asarray(val_macro_F1_list))

    if model_ema is not None:
        if args.cls_problem == 'covid_cls':

            with open(os.path.join(args.checkpoint_output, 'train_sensitivity_model_ema.npy'), 'w') as outfile:
                np.save(outfile.name, np.asarray(train_sensitivity_list_model_ema))

            with open(os.path.join(args.checkpoint_output, 'train_specificity_model_ema.npy'), 'w') as outfile:
                np.save(outfile.name, np.asarray(train_specificity_list_model_ema))

            with open(os.path.join(args.checkpoint_output, 'train_auc_model_ema.npy'), 'w') as outfile:
                np.save(outfile.name, np.asarray(train_AUC_list_model_ema))

            with open(os.path.join(args.checkpoint_output, 'train_pr_score_model_ema.npy'), 'w') as outfile:
                np.save(outfile.name, np.asarray(train_PRS_list_model_ema))

            with open(os.path.join(args.checkpoint_output, 'val_sensitivity_model_ema.npy'), 'w') as outfile:
                np.save(outfile.name, np.asarray(val_sensitivity_list_model_ema))

            with open(os.path.join(args.checkpoint_output, 'val_specificity_model_ema.npy'), 'w') as outfile:
                np.save(outfile.name, np.asarray(val_specificity_list_model_ema))

            with open(os.path.join(args.checkpoint_output, 'val_auc_model_ema.npy'), 'w') as outfile:
                np.save(outfile.name, np.asarray(val_AUC_list_model_ema))

            with open(os.path.join(args.checkpoint_output, 'val_pr_score_model_ema.npy'), 'w') as outfile:
                np.save(outfile.name, np.asarray(val_PRS_list_model_ema))

        with open(os.path.join(args.checkpoint_output, 'train_acc_model_ema.npy'), 'w') as outfile:
            np.save(outfile.name, np.asarray(train_accuracy_list_model_ema))

        with open(os.path.join(args.checkpoint_output, 'train_loss_model_ema.npy'), 'w') as outfile:
            np.save(outfile.name, np.asarray(train_loss_list_model_ema))

        with open(os.path.join(args.checkpoint_output, 'train_f1_score_model_ema.npy'), 'w') as outfile:
            np.save(outfile.name, np.asarray(train_macro_F1_list_model_ema))

        with open(os.path.join(args.checkpoint_output, 'val_acc_model_ema.npy'), 'w') as outfile:
            np.save(outfile.name, np.asarray(val_accuracy_list_model_ema))

        with open(os.path.join(args.checkpoint_output, 'val_loss_model_ema.npy'), 'w') as outfile:
            np.save(outfile.name, np.asarray(val_loss_list_model_ema))

        with open(os.path.join(args.checkpoint_output, 'val_f1_score_model_ema.npy'), 'w') as outfile:
            np.save(outfile.name, np.asarray(val_macro_F1_list_model_ema))


    print('Total Training Time: %.2f min...\n' % ((time.time() - start_time) / 60))

@torch.no_grad()
def validate_model(args, epoch, model, val_loader, criterion, writer, is_model_ema=False):
    device = args.device

    val_loss = 0

    correct_pred = 0

    num_examples = 0

    with torch.no_grad():

        model.eval()

        for batch_idx, batch_data in enumerate(val_loader):

            inputs, targets = batch_data['image_ct'].to(device), batch_data['label'].to(device)

            # y = F.one_hot(targets.unsqueeze(0).to(torch.int64), num_classes=args.num_classes).view(targets.shape[0],args.num_classes)
            inputs = repeat(inputs, 'b c h w d ->b (repeat c) h w d ',
                            repeat=3)  ##we use this to keep 3 channels input for pretrained models
            # print(inputs.shape)
            inputs = rearrange(inputs, 'b c h w d -> b c d h w')

            if args.proj_distance_weight >= 0:
                outputs,_ = model(inputs)###dirty solution to work around as the ema is in eval mode
            else:
                outputs = model(inputs)###to train only the MT with non contrastive loss....
            batch_loss = criterion(outputs, targets.long())

            val_loss += batch_loss

            probas = F.softmax(outputs, dim=1)

            predicted_labels = outputs.argmax(dim=1)

            num_examples += len(targets)
            correct_pred += torch.eq(predicted_labels, targets).sum().item()

            if batch_idx == 0:
                scores = probas.detach().cpu().numpy()
                labels = targets.detach().cpu().numpy()
            else:
                scores = np.concatenate((scores, probas.detach().cpu().numpy()), axis=0)
                labels = np.concatenate((labels, targets.detach().cpu().numpy()))

        metric_collects = utils.calc_multi_cls_measures_ECCV2022_Challenge(args.cls_problem,scores, labels)
        loss = val_loss / num_examples
        if args.cls_problem=='covid_cls':
            writer.add_scalar('Val_Loss_model_ema_{}'.format(str(is_model_ema)), loss, epoch + 1)
            writer.add_scalar('Val_ACC_model_ema_{}'.format(str(is_model_ema)), metric_collects['accuracy'], epoch + 1)
            writer.add_scalar('Val_AUC_model_ema_{}'.format(str(is_model_ema)), metric_collects['AUC'], epoch + 1)
            writer.add_scalar('Val_macro_f1_model_ema_{}'.format(str(is_model_ema)), metric_collects['macro_f1'], epoch + 1)
            writer.add_scalar('Val_AVE_Pr_score_model_ema_{}'.format(str(is_model_ema)), metric_collects['Ave_pr_score'], epoch + 1)

            wandb.log({
                "Val_Loss_model_ema_{}".format(str(is_model_ema)): loss,
                "Val_ACC_model_ema_{}".format(str(is_model_ema)): metric_collects['accuracy'],
                "Val_AUC_model_ema_{}".format(str(is_model_ema)): metric_collects['AUC'],
                "Val_macro_f1_model_ema_{}".format(str(is_model_ema)): metric_collects['macro_f1'],
                "Val_AVE_Pr_score_model_ema_{}".format(str(is_model_ema)): metric_collects['Ave_pr_score'],
            })
            utils.print_epoch_metrics_ECCV2022_Challenge(args, epoch=epoch + 1, metric_collects=metric_collects,
                                                         stage='Validation_model_ema_{}'.format(str(is_model_ema)),
                                                         loss=loss)
        elif args.cls_problem=='covid_severity_cls':
            writer.add_scalar('Val_Loss_model_ema_{}'.format(str(is_model_ema)), loss, epoch + 1)
            writer.add_scalar('Val_ACC_model_ema_{}'.format(str(is_model_ema)), metric_collects['accuracy'],
                                  epoch + 1)
            writer.add_scalar('Val_macro_f1_model_ema_{}'.format(str(is_model_ema)), metric_collects['macro_f1'],
                                  epoch + 1)

            wandb.log({
                    "Val_Loss_model_ema_{}".format(str(is_model_ema)): loss,
                    "Val_ACC_model_ema_{}".format(str(is_model_ema)): metric_collects['accuracy'],
                    "Val_macro_f1_model_ema_{}".format(str(is_model_ema)): metric_collects['macro_f1'],
                })

        utils.print_epoch_metrics_ECCV2022_Challenge(args, epoch=epoch + 1, metric_collects=metric_collects,
                                      stage='Validation_model_ema_{}'.format(str(is_model_ema)),loss=loss)
    return metric_collects, loss
