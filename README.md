# DRN ConvNeXt COVID-19

Source code of the model presented in the preprint [[`arXiv`](https://arxiv.org/pdf/2207.01437.pdf)
] **Representation Learning with Information Theory for COVID-19 Detection** 

This PyTorch implementation corresponds to the solution implemented to benchmark our model on the 2nd Covid-19 Competition, occurring in the framework of the AI-MIA Workshop in the European Conference
on Computer Vision (ECCV 2022).

![alt text](img/method_overview.png "Schema of the proposed method.")


## License
This repository is released under the MIT license as found in the [`LICENSE`](https://gitlab.com/aberenguer/MT-ConvNeXt-COVID-19/-/blob/main/LICENSE) file.
